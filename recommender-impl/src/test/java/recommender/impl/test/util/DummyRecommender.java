/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.util;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.Recommender;
import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;
import recommender.core.util.RecommendationResultComparator;

/**
 * Dummy recommender for simulating different latency periods.
 * 
 * @author fei
 * @param <E> 
 * @param <R> 
 */
public abstract class DummyRecommender<E, R extends RecommendationResult> implements RecommenderConnector<E, R> {
	private static final Log log = LogFactory.getLog(DummyRecommender.class);
	
	private RecommendationRenderer<E, R> renderer;
	private Random random = new Random();
	private long wait = (long) (Math.random() * 1000); 
	private Integer id = Integer.valueOf(0);
	private int numberOfTagsToRecommend = Recommender.DEFAULT_NUMBER_OF_RESULTS_TO_RECOMMEND;
	
	/**
	 * 
	 */
	public DummyRecommender() {
		this.random.setSeed(14214134);
	}
	
	/**  
	 * Do nothing.
	 * @see recommender.core.Recommender#addRecommendation(java.util.Collection, recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public void addRecommendation(Collection<R> recommendedTags, E entity) {
		log.info("Dummy recommender: addRecommendedTags.");

		// create informative recommendation:
		for (int i = 0; i < numberOfTagsToRecommend; i++) {
			double score = this.random.nextDouble();
			double confidence = this.random.nextDouble();
			DecimalFormat df = new DecimalFormat("0.00");
			String re = "Dummy (" + df.format(score) + "," + df.format(confidence) + "[" + getWait() + "])";
			recommendedTags.add(this.createRecommenationResult(re, score, confidence));
		}
		
		try {
			Thread.sleep(getWait());
		} catch (InterruptedException e) {
			// nothing to do.
		}
		
	}

	/**
	 * @param re
	 * @param score TODO
	 * @param confidence TODO
	 * @return a recommendation result
	 */
	protected abstract R createRecommenationResult(String re, double score, double confidence);

	@Override
	public String getInfo() {
		return "Dummy recommender which does nothing at all.";
	}

	@Override
	public SortedSet<R> getRecommendation(E entity) {
		final SortedSet<R> recommendedTags = new TreeSet<R>(new RecommendationResultComparator<R>());
		addRecommendation(recommendedTags, entity);
		return recommendedTags;
	}

	@Override
	public String getId() {
		return "DummyTagRecommender-"+id;
	}

	@Override
	public void setFeedback(E entity, R tag) {
		/*
		 * this recommender ignores feedback
		 */
	}

	/**
	 * @return the wait
	 */
	public long getWait() {
		return this.wait;
	}

	/**
	 * @param wait the wait to set
	 */
	public void setWait(long wait) {
		this.wait = wait;
	}	
	
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	

	@Override
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend) {
		this.numberOfTagsToRecommend = numberOfResultsToRecommend;	
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.RecommenderConnector#getRecommendationRenderer()
	 */
	@Override
	public RecommendationRenderer<E, R> getRecommendationRenderer() {
		return this.renderer;
	}

	/* (non-Javadoc)
	 * @see recommender.core.interfaces.RecommenderConnector#setRecommendationRenderer(recommender.core.interfaces.renderer.RecommendationRenderer)
	 */
	@Override
	public void setRecommendationRenderer(RecommendationRenderer<E, R> renderer) {
		this.renderer = renderer;
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.RecommenderConnector#isTrusted()
	 */
	@Override
	public boolean isTrusted() {
		return false;
	}
}
