/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.util;

import java.util.SortedSet;
import java.util.TreeSet;

import recommender.core.interfaces.model.AbstractRecommendationResult;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.util.RecommendationResultComparator;

/**
 * TODO: add documentation to this class
 *
 * @author dzo
 */
public class DummyRecommendationResult extends AbstractRecommendationResult {
	
	private String id;
	private String title;
	
	/**
	 * 
	 */
	public DummyRecommendationResult() {
		super();
	}

	/**
	 * @param score
	 * @param confidence
	 */
	public DummyRecommendationResult(double score, double confidence) {
		super(score, confidence);
	}

	/**
	 * @param title
	 * @param score
	 * @param confidence
	 */
	public DummyRecommendationResult(String title, double score, double confidence) {
		super(score, confidence);
		this.id = title;
		this.title = title;
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.model.RecommendationResult#compareToOtherRecommendationResult(recommender.core.interfaces.model.RecommendationResult)
	 */
	@Override
	public int compareToOtherRecommendationResult(RecommendationResult o) {
		return this.title.toLowerCase().compareTo(o.getTitle().toLowerCase());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	
	// FIXME: (dzo): was not clear that equals has to be overriden
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DummyRecommendationResult other = (DummyRecommendationResult) obj;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see recommender.core.interfaces.model.RecommendationResult#getId()
	 */
	@Override
	public String getRecommendationId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see recommender.core.interfaces.model.RecommendationResult#getTitle()
	 */
	@Override
	public String getTitle() {
		return this.title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 
	 * @param titles
	 * @param fixedConfidences
	 * @return
	 */
	public static SortedSet<DummyRecommendationResult>  getDummyRecommendationResults(String[] titles, double fixedConfidences) {
		final double[] scores = new double[titles.length];
		
		for (int i = 0; i < titles.length; i++) {
			scores[i] = 1.0 / (1.0 + i);
		}
		
		return getDummyRecommendationResults(titles, scores, new double[] { fixedConfidences });
	}

	/**
	 * @param titles
	 * @param scores
	 * @param confidences
	 * @return
	 */
	public static SortedSet<DummyRecommendationResult> getDummyRecommendationResults(String[] titles, double[] scores, double[] confidences) {
		final SortedSet<DummyRecommendationResult> results = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<RecommendationResult>());
		
		for (int i = 0; i < titles.length; i++) {
			final DummyRecommendationResult dummyRecommendationResult = new DummyRecommendationResult();
			dummyRecommendationResult.setTitle(titles[i]);
			dummyRecommendationResult.setScore(i < scores.length ? scores[i] : scores[0]);
			dummyRecommendationResult.setConfidence(i < confidences.length ? confidences[i] : confidences[0]);
			
			results.add(dummyRecommendationResult);
		}
		
		return results;
	}

}
