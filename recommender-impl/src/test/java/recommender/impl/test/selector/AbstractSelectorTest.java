/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.selector;

import java.util.Random;
import java.util.SortedSet;

import org.bibsonomy.recommender.simple.FixedRecommender;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.impl.multiplexer.RecommendationResultManager;
import recommender.impl.test.testutil.DummyDBLogic;
import recommender.impl.test.util.DummyRecommendationEntity;
import recommender.impl.test.util.DummyRecommendationResult;

/**
 * This class provides general preparation for selector tests.
 * Tagrecommendations are calculated, but the result of this test should also be 
 * meaningful for itemrecommendations, because it only tests the mechanisms.
 * 
 * @author lukas
 *
 */
public abstract class AbstractSelectorTest {

	protected static final String PRIMARY_TAG_TITLE = "itemOfPrimary";
	protected static final String FALLBACK_TAG_TITLE = "itemOfFallback";

	protected DummyDBLogic<DummyRecommendationEntity, DummyRecommendationResult> dbLogic;
	protected RecommendationResultManager<DummyRecommendationEntity, DummyRecommendationResult> cache;
	protected SortedSet<DummyRecommendationResult> primaryResults;
	protected SortedSet<DummyRecommendationResult> fallbackResults;
	protected Long primarySid;
	protected Long fallbackSid;
	protected Long qid;
	
	/**
	 * This method initializes the database logic and the result cache.
	 * It also triggers the recommendation calculation on the given
	 * recommender and adds the results to cache and database.
	 * 
	 * @param primaryRecommender the primary test recommender
	 * @param fallbackRecommender the fallback test recommender
	 */
	protected void prepareForSelectorTest(final Recommender<DummyRecommendationEntity, DummyRecommendationResult> primaryRecommender, final Recommender<DummyRecommendationEntity, DummyRecommendationResult> fallbackRecommender) {
		
		// initializing
		dbLogic = new DummyDBLogic<DummyRecommendationEntity, DummyRecommendationResult>();
		
		cache = new RecommendationResultManager<DummyRecommendationEntity, DummyRecommendationResult>();
		
		final Random random = new Random();
		random.setSeed(1023123);
		qid = Long.valueOf(random.nextLong());
		
		cache.startQuery(qid);
		
		dbLogic.registerRecommender(primaryRecommender);
		dbLogic.registerRecommender(fallbackRecommender);
		
		primarySid = dbLogic.getRecommenderId(primaryRecommender);
		fallbackSid = dbLogic.getRecommenderId(fallbackRecommender);
		
		// calculate results and add them to the dummy logic
		primaryResults = primaryRecommender.getRecommendation(null);
		cache.addResult(qid, primarySid, primaryResults);
		dbLogic.addRecommendation(qid, primarySid, primaryResults, 0);
		fallbackResults = fallbackRecommender.getRecommendation(null);
		cache.addResult(qid, fallbackSid, fallbackResults);
		dbLogic.addRecommendation(qid, fallbackSid, fallbackResults, 0);
		
		cache.stopQuery(qid);
	}
	
	protected class Fixed2Recommender<E, R extends RecommendationResult> extends FixedRecommender<E, R> {

		/**
		 * @param results
		 */
		public Fixed2Recommender(SortedSet<R> results) {
			super(results);
		}
	}
}
