/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.selector;

import static org.junit.Assert.assertEquals;

import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.recommender.simple.FixedRecommender;
import org.junit.Test;

import recommender.core.util.RecommendationResultComparator;
import recommender.impl.multiplexer.strategy.SelectFixRecommenderWithFallback;
import recommender.impl.test.util.DummyRecommendationEntity;
import recommender.impl.test.util.DummyRecommendationResult;

/**
 * This class tests the mechanism of {@link SelectFixRecommenderWithFallback}.
 * Tagrecommendations are calculated, but the result of this test should also be 
 * meaningful for itemrecommendations. 
 * 
 * @author lukas
 */
public class SelectFixRecommenderWithFallbackTest extends AbstractSelectorTest {

	/**
	 * Tests whether the fallback is chosen in case the primary didn't return any recommendations.
	 */
	@Test
	public void testFixWithFallbackFallbackForTags() {
		// primary will deliver no results
		final SortedSet<DummyRecommendationResult> tagsOfPrimary = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> primaryRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(tagsOfPrimary);
		
		final SortedSet<DummyRecommendationResult> tagsOfFallback = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		tagsOfFallback.add(new DummyRecommendationResult(FALLBACK_TAG_TITLE, 1.0, 0.0));
		final Fixed2Recommender<DummyRecommendationEntity, DummyRecommendationResult> fallbackRecommender = new Fixed2Recommender<DummyRecommendationEntity, DummyRecommendationResult>(tagsOfFallback);
		
		this.prepareForSelectorTest(primaryRecommender, fallbackRecommender);
		
		final SelectFixRecommenderWithFallback<DummyRecommendationEntity, DummyRecommendationResult> selector = new SelectFixRecommenderWithFallback<DummyRecommendationEntity, DummyRecommendationResult>();
		selector.setDbLogic(dbLogic);
		selector.setPrimaryRecommender(primaryRecommender);
		selector.setFallbackRecommender(fallbackRecommender);
		
		final SortedSet<DummyRecommendationResult> results = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		selector.selectResult(qid, cache, results);
		
		cache.releaseQuery(qid);
		
		// the item given to the fallback recommender should be the only one in
		// the result set, because the primary did not return any recommendations
		assertEquals(this.fallbackResults.size(), results.size());
		assertEquals(FALLBACK_TAG_TITLE, results.first().getTitle());
	}
	
	/**
	 * Tests whether the primary is chosen in case it returned recommendations.
	 */
	@Test
	public void testFixWithFallbackPrimaryForTags() {
		// primary will deliver results
		SortedSet<DummyRecommendationResult> tagsOfPrimary = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		tagsOfPrimary.add(new DummyRecommendationResult(PRIMARY_TAG_TITLE, 1.0, 0.0));
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> primaryRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(tagsOfPrimary);
		
		
		final SortedSet<DummyRecommendationResult> tagsOfFallback = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		tagsOfFallback.add(new DummyRecommendationResult(FALLBACK_TAG_TITLE, 1.0, 0.0));
		final Fixed2Recommender<DummyRecommendationEntity, DummyRecommendationResult> fallbackRecommender = new Fixed2Recommender<DummyRecommendationEntity, DummyRecommendationResult>(tagsOfFallback);
		
		this.prepareForSelectorTest(primaryRecommender, fallbackRecommender);
		
		final SelectFixRecommenderWithFallback<DummyRecommendationEntity, DummyRecommendationResult> selector = new SelectFixRecommenderWithFallback<DummyRecommendationEntity, DummyRecommendationResult>();
		selector.setDbLogic(dbLogic);
		selector.setPrimaryRecommender(primaryRecommender);
		selector.setFallbackRecommender(fallbackRecommender);
		
		SortedSet<DummyRecommendationResult> results = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		selector.selectResult(qid, cache, results);
		
		cache.releaseQuery(qid);
		
		// the item given to the primary recommender should be the only one in
		// the result set
		assertEquals(this.primaryResults.size(), results.size());
		assertEquals(PRIMARY_TAG_TITLE, results.first().getTitle());
	}
}
