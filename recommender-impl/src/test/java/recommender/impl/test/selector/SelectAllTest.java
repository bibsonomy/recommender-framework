/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.selector;

import static org.junit.Assert.assertEquals;

import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.recommender.simple.FixedRecommender;
import org.junit.Test;

import recommender.core.util.RecommendationResultComparator;
import recommender.impl.multiplexer.strategy.SelectAll;
import recommender.impl.test.util.DummyRecommendationEntity;
import recommender.impl.test.util.DummyRecommendationResult;

/**
 * This class tests the mechanism of {@link SelectAll}.
 * Tagrecommendations are calculated, but the result of this test should also be 
 * meaningful for itemrecommendations. 
 * 
 * @author lukas
 *
 */
public class SelectAllTest extends AbstractSelectorTest {

	/**
	 * Tests the case that two recommenders delivered results, so the selection should be the union
	 * of both sets.
	 */
	@Test
	public void testSelectAll() {
		// primary will deliver results
		final SortedSet<DummyRecommendationResult> tagsOfPrimary = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		tagsOfPrimary.add(new DummyRecommendationResult(PRIMARY_TAG_TITLE, 1.0, 0.0));
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> primaryRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(tagsOfPrimary);

		// fallback also, so the results should be integrated
		final SortedSet<DummyRecommendationResult> tagsOfFallback = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		tagsOfFallback.add(new DummyRecommendationResult(FALLBACK_TAG_TITLE, 1.0, 0.0));
		final Fixed2Recommender<DummyRecommendationEntity, DummyRecommendationResult> fallbackRecommender = new Fixed2Recommender<DummyRecommendationEntity, DummyRecommendationResult>(tagsOfFallback);

		this.prepareForSelectorTest(primaryRecommender, fallbackRecommender);

		final SelectAll<DummyRecommendationEntity, DummyRecommendationResult> selector = new SelectAll<DummyRecommendationEntity, DummyRecommendationResult>();
		selector.setDbLogic(dbLogic);

		// the primary recommender was selected most
		dbLogic.addSelectionCountForRecommender(primarySid, Long.valueOf(12L));
		dbLogic.addSelectionCountForRecommender(fallbackSid, Long.valueOf(6L));
		
		cache.stopQuery(qid);

		final SortedSet<DummyRecommendationResult> results = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		selector.selectResult(qid, cache, results);

		cache.releaseQuery(qid);

		// the results should contain all recommendations of each recommender
		assertEquals(this.primaryResults.size() + this.fallbackResults.size(), results.size());
	}

}
