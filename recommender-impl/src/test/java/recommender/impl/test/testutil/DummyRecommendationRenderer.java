/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.testutil;

import java.io.Reader;
import java.io.Writer;
import java.util.SortedSet;
import java.util.TreeSet;

import recommender.core.error.BadRequestOrResponseException;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;

/**
 * Dummy implementation of the renderer. Returns null on most methods.
 * 
 * @author lukas
 *
 * @param <E>
 * @param <R>
 */
public class DummyRecommendationRenderer<E, R extends RecommendationResult> implements RecommendationRenderer<E, R> {

	@Override
	public void serializeRecommendationEntity(Writer writer, E entity) {
		// do nothing
	}

	@Override
	public SortedSet<R> parseRecommendationResultList(Reader reader) throws BadRequestOrResponseException {
		return new TreeSet<R>();
	}

	@Override
	public String parseStat(Reader reader) throws BadRequestOrResponseException {
		return null;
	}

	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#parseRecommendationEntity(java.io.Reader)
	 */
	@Override
	public E parseRecommendationEntity(Reader reader) {
		return null;
	}

	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#serializeRecommendationResultList(java.io.Writer, java.util.SortedSet)
	 */
	@Override
	public void serializeRecommendationResultList(Writer writer, SortedSet<R> recommendations) {
		// noop
	}

	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#getContentType()
	 */
	@Override
	public String getContentType() {
		return null;
	}

	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#serializeOK(java.io.Writer)
	 */
	@Override
	public void serializeOK(Writer writer) {
		// noop
	}

}
