/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.testutil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import recommender.core.Recommender;
import recommender.core.database.DBLogic;
import recommender.core.database.params.RecAdminOverview;
import recommender.core.database.params.RecQueryParam;
import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.model.Pair;
import recommender.core.util.RecommenderUtil;

/**
 * TODO: add documentation to this class
 *
 * @author fei, lha
 * @param <E> 
 * @param <R> 
 */
public class DummyDBLogic<E, R extends RecommendationResult> implements DBLogic<E, R> {

	// contains the count of delivered results, in a pair with the recommender's id
	private List<Pair<Long, Long>> recommenderSelectionCount = new ArrayList<Pair<Long,Long>>();
	// contains the results of the recommendations, identified by the recommender's id
	private Map<Long, SortedSet<R>> recommendations = new HashMap<Long, SortedSet<R>>();
	// contains the setting ids of the added recommenders
	private Map<String, Long> recommenderSettingIds = new HashMap<String, Long>();
	
	// for generating ids
	private long id_generator = 0L;
	private Set<Recommender<E, R>> activeRecommenders = new HashSet<Recommender<E,R>>();
	
	@Override
	public Long addQuery(String userName, Date date, E entity, int timeout) {
		return (long) (Math.random() * System.currentTimeMillis());
	}

	@Override
	public void addRecommenderToQuery(Long qid, Long sid) {
		// do nothing
	}

	@Override
	public void setResultSelectorToQuery(Long qid, Long rid) {
		// do nothing
	}

	@Override
	public Long insertSelectorSetting(String selectorInfo, byte[] selectorMeta) {
		return this.getRandomId();
	}

	@Override
	public void addSelectedRecommender(Long qid, Long sid) {
		// do nothing
		
	}

	@Override
	public int addRecommendation(Long queryId, Long settingsId,
			SortedSet<R> recommendationResults, long latency) {
		this.recommendations.put(settingsId, recommendationResults);
		return recommendationResults.size();
	}

	@Override
	public SortedSet<R> getRecommendations(Long qid, Long sid) {
		return this.recommendations.get(sid);
	}

	@Override
	public void getRecommendations(Long qid, Long sid,
			Collection<R> recommendationResults) {
		recommendationResults.addAll(this.recommendations.get(sid));
	}

	@Override
	public SortedSet<R> getRecommendations(Long qid) {
		final SortedSet<R> results = new TreeSet<R>();
		this.getRecommendations(qid, results);
		return results;
	}

	@Override
	public void getRecommendations(Long qid, Collection<R> recommendationResults) {
		for(Long sid : recommendations.keySet()) {
			recommendationResults.addAll(recommendations.get(sid));
		}
	}

	@Override
	public List<R> getSelectedResults(Long qid) {
		// do nothing
		return null;
	}

	@Override
	public List<Long> getActiveRecommenderIDs(Long qid) {
		final List<Long> ids = new ArrayList<Long>();
		for(Long sid : recommendations.keySet()) {
			if(recommendations.get(sid).size() > 0) {
				ids.add(sid);
			}
		}
		return ids;
	}

	@Override
	public List<Long> getAllRecommenderIDs(Long qid) {
		final List<Long> ids = new ArrayList<Long>();
		for(String recommenderName : recommenderSettingIds.keySet()) {
			ids.add(recommenderSettingIds.get(recommenderName));
		}
		return ids;
	}

	@Override
	public List<Pair<Long, Long>> getRecommenderSelectionCount(Long qid) {
		return this.recommenderSelectionCount;
	}

	@Override
	public RecQueryParam getQuery(Long qid) {
		// do nothing
		return null;
	}

	@Override
	public RecAdminOverview getRecommenderAdminOverview(String id) {
		// do nothing
		return null;
	}

	@Override
	public Long getAverageLatencyForRecommender(Long sid, Long numberOfQueries) {
		// do nothing
		return null;
	}

	@Override
	public void updateRecommenderstatus(List<Long> activeRecs, List<Long> disabledRecs) {
		// do nothing
	}
	
	@Override
	public int storeRecommendation(Long qid, Long rid, Collection<R> result) {
		// do nothing
		return 0;
	}

	@Override
	public Long getQueryForEntity(String user_name, Date date, String entityID) {
		// do nothing
		return null;
	}

	@Override
	public void addFeedback(String userName, E entity, R result) {
		// do nothing
	}
	
	/**
	 * @return a temporary unique id
	 */
	private Long getRandomId() {
		id_generator++;
		if (id_generator == Long.MAX_VALUE) {
			id_generator = 0L;
		}
		return Long.valueOf(id_generator);
	}
	
	/**
	 * Ads manually the selection count of a recommender for testing purpose.
	 * 
	 * @param sid the sid of the recommender
	 * @param selectionCount the count of selections of the recommender given by sid
	 */
	public void addSelectionCountForRecommender(final Long sid, final Long selectionCount) {
		this.recommenderSelectionCount.add(new Pair<Long, Long>(sid, selectionCount));
	}

	/* (non-Javadoc)
	 * @see recommender.core.database.DBLogic#isRecommenderRegistered(recommender.core.Recommender)
	 */
	@Override
	public boolean isRecommenderRegistered(final Recommender<E, R> recommender) {
		return this.getRecommenderId(recommender) != null;
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.database.DBLogic#getRecommenderId(recommender.core.Recommender)
	 */
	@Override
	public Long getRecommenderId(Recommender<E, R> recommender) {
		return this.recommenderSettingIds.get(RecommenderUtil.getRecommenderId(recommender));
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.database.DBLogic#getRemoteRecommenders()
	 */
	@Override
	public List<RecommenderConnector<E, R>> getRemoteRecommenders() {
		return Collections.emptyList();
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.database.DBLogic#isRecommenderActive(recommender.core.Recommender)
	 */
	@Override
	public boolean isRecommenderActive(Recommender<E, R> recommender) {
		return this.activeRecommenders.contains(recommender);
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.database.DBLogic#registerRecommender(recommender.core.Recommender)
	 */
	@Override
	public void registerRecommender(Recommender<E, R> recommender) {
		final Long sid = this.getRandomId();
		this.recommenderSettingIds.put(RecommenderUtil.getRecommenderId(recommender), sid);
		this.activeRecommenders.add(recommender);
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.database.DBLogic#removeRecommender(recommender.core.Recommender)
	 */
	@Override
	public boolean removeRecommender(Recommender<E, R> recommender) {
		// TODO Auto-generated method stub
		return false;
	}
}
