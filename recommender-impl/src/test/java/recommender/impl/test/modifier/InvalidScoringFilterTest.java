/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.test.modifier;

import static org.junit.Assert.assertEquals;

import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Test;

import recommender.core.util.RecommendationResultComparator;
import recommender.impl.modifiers.InvalidScoringFilter;
import recommender.impl.test.util.DummyRecommendationResult;

/**
 * This class tests the mechanism of the {@link InvalidScoringFilter}.
 * This test uses tag recommendations, but the result is also representative
 * for item recommendation filtering.
 * 
 * @author lukas
 *
 */
public class InvalidScoringFilterTest {

	private static final double DELTA = 0.1;
	
	/**
	 * Tests each possible invalid value and it's substitution.
	 */
	@Test
	public void testInvalidScoringFilter() {
		final DummyRecommendationResult tag = new DummyRecommendationResult("foo", Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
		final InvalidScoringFilter<DummyRecommendationResult> filter = new InvalidScoringFilter<DummyRecommendationResult>();
		
		final SortedSet<DummyRecommendationResult> tags = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		tags.add(tag);
		
		filter.alterResult(tags);
		
		assertEquals(Integer.MAX_VALUE, tags.first().getScore(), DELTA);
		assertEquals(Integer.MAX_VALUE, tags.first().getConfidence(), DELTA);
		
		tag.setScore(Double.NEGATIVE_INFINITY);
		tag.setConfidence(Double.NEGATIVE_INFINITY);
		
		filter.alterResult(tags);
		
		assertEquals(Integer.MIN_VALUE, tags.first().getScore(), DELTA);
		assertEquals(Integer.MIN_VALUE, tags.first().getConfidence(), DELTA);
		
		tag.setScore(Double.NaN);
		tag.setConfidence(Double.NaN);
		
		filter.alterResult(tags);
		
		assertEquals(Integer.MIN_VALUE, tags.first().getScore(), DELTA);
		assertEquals(Integer.MIN_VALUE, tags.first().getConfidence(), DELTA);
	}
}
