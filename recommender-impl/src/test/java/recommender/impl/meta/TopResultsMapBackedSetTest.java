/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Test;

import recommender.core.util.RecommendationResultComparator;
import recommender.impl.test.util.DummyRecommendationResult;


/**
 * @author rja
 */
public class TopResultsMapBackedSetTest {
	
	/**
	 * tests {@link TopResultsMapBackedSet}
	 */
	@Test
	public void testMapBackedSetWithRecommendedTags() {
		final Set<DummyRecommendationResult> tempSet = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>()); // all tags
		final Set<DummyRecommendationResult> topSet = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());  // top tags

		tempSet.add(new DummyRecommendationResult("a", 0.1, 0.5));
		tempSet.add(new DummyRecommendationResult("b", 0.2, 0.5));
		tempSet.add(new DummyRecommendationResult("c", 0.3, 0.5));

		topSet.add(new DummyRecommendationResult("d", 0.4, 0.5));
		topSet.add(new DummyRecommendationResult("e", 0.5, 0.5));
		topSet.add(new DummyRecommendationResult("f", 0.6, 0.5));
		topSet.add(new DummyRecommendationResult("g", 0.7, 0.5));
		topSet.add(new DummyRecommendationResult("h", 0.8, 0.5));


		tempSet.addAll(topSet);

		final TopResultsMapBackedSet<DummyRecommendationResult> set = new TopResultsMapBackedSet<DummyRecommendationResult>(5);
		for (final DummyRecommendationResult recommendedTag : tempSet) {
			set.add(recommendedTag);
		}

		/*
		 * sets should be equal, basically ...
		 */
		assertTrue(set.containsAll(tempSet));
		assertTrue(tempSet.containsAll(set));

		/*
		 * we should get the top five tags ...
		 */
		final SortedSet<DummyRecommendationResult> sortedTags = set.getTopResults();
		assertEquals(5, sortedTags.size());

		assertTrue(topSet.containsAll(sortedTags));
		assertTrue(sortedTags.containsAll(topSet));
	}
}
