/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.recommender.simple.FixedRecommender;
import org.junit.Test;

import recommender.core.util.RecommendationResultComparator;
import recommender.impl.test.util.DummyRecommendationEntity;
import recommender.impl.test.util.DummyRecommendationResult;

/**
 * @author rja
 */
public class ResultsFromFirstWeightedBySecondFilledByThirdRecommenderTest {
	
	@Test
	public void testAddRecommendedTags() {
		final String[] firstFixedTags = new String[]{"eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "eins"};
		final SortedSet<DummyRecommendationResult> firstFixedTagSet = DummyRecommendationResult.getDummyRecommendationResults(firstFixedTags, 0.5);
		
		final SortedSet<DummyRecommendationResult> secondFixedTags = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		secondFixedTags.add(new DummyRecommendationResult("eins", 0.3, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("drei", 0.2, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("vier", 0.5, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("sieben", 0.6, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("eins", 0.5, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("eins", 0.2, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("semantic", 0.5, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("bar", 0.6, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("foo", 0.7, 0.2));
		secondFixedTags.add(new DummyRecommendationResult("net", 0.8, 0.2));


		final ResultsFromFirstWeightedBySecondFilledByThirdRecommender<DummyRecommendationEntity, DummyRecommendationResult> merger = new ResultsFromFirstWeightedBySecondFilledByThirdRecommender<DummyRecommendationEntity, DummyRecommendationResult>();

		merger.setFirstRecommender(new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(firstFixedTagSet));
		merger.setSecondRecommender(new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(secondFixedTags));
		merger.setThirdRecommender(new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(secondFixedTags));
		merger.setNumberOfResultsToRecommend(5);

		final SortedSet<DummyRecommendationResult> recommendedTags = merger.getRecommendation(null);


		/*
		 *  check containment and order of top results
		 */
		final Iterator<DummyRecommendationResult> iterator = recommendedTags.iterator();
		assertEquals("sieben", iterator.next().getTitle());
		assertEquals("vier", iterator.next().getTitle());
		assertEquals("eins", iterator.next().getTitle());
		assertEquals("drei", iterator.next().getTitle());
		assertEquals("zwei", iterator.next().getTitle());
		assertFalse(iterator.hasNext());
	}

	@Test
	public void test2() {
		final SortedSet<DummyRecommendationResult> recommended = DummyRecommendationResult.getDummyRecommendationResults(new String[]{"semantic", "web", "social", "net", "graph", "tool", "folksonomy", "holiday"}, 0.5);
		final SortedSet<DummyRecommendationResult> titleResult = DummyRecommendationResult.getDummyRecommendationResults(new String[]{"nepomuk", "social", "semantic", "desktop"}, 0.5);
		
		final ResultsFromFirstWeightedBySecondFilledByThirdRecommender<DummyRecommendationEntity, DummyRecommendationResult> merger = new ResultsFromFirstWeightedBySecondFilledByThirdRecommender<DummyRecommendationEntity, DummyRecommendationResult>();
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> simpleContentBasedTagRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(titleResult);
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> fixedTagsTagRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(recommended);

		merger.setFirstRecommender(simpleContentBasedTagRecommender);
		merger.setSecondRecommender(fixedTagsTagRecommender);
		merger.setThirdRecommender(fixedTagsTagRecommender);
		merger.setNumberOfResultsToRecommend(5);
		
		final SortedSet<DummyRecommendationResult> recommendedTags = merger.getRecommendation(null);

		/*
		 *  check containment and order of top tags
		 */
		final Iterator<DummyRecommendationResult> iterator = recommendedTags.iterator();
		assertEquals("semantic", iterator.next().getTitle());
		assertEquals("social", iterator.next().getTitle());
		assertEquals("nepomuk", iterator.next().getTitle());
		assertEquals("desktop", iterator.next().getTitle());
		assertEquals("web", iterator.next().getTitle());
		assertFalse(iterator.hasNext());
	}


	@Test
	public void test3() {
		final SortedSet<DummyRecommendationResult> usersTags = DummyRecommendationResult.getDummyRecommendationResults(new String[]{"semantic", "web", "social", "net", "graph", "tool", "folksonomy", "holiday"}, 0.5);
		final SortedSet<DummyRecommendationResult> resourceTags = DummyRecommendationResult.getDummyRecommendationResults(new String[]{"project"}, 0.5);
		final SortedSet<DummyRecommendationResult> titleResult = DummyRecommendationResult.getDummyRecommendationResults(new String[]{"nepomuk", "social", "semantic", "desktop"}, 0.5);

		final ResultsFromFirstWeightedBySecondFilledByThirdRecommender<DummyRecommendationEntity, DummyRecommendationResult> merger = new ResultsFromFirstWeightedBySecondFilledByThirdRecommender<DummyRecommendationEntity, DummyRecommendationResult>();
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> simpleContentBasedTagRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(titleResult);
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> fixedTagsTagRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(usersTags);
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> fillupTagRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(resourceTags);

		merger.setFirstRecommender(simpleContentBasedTagRecommender);
		merger.setSecondRecommender(fixedTagsTagRecommender);
		merger.setThirdRecommender(fillupTagRecommender);
		merger.setNumberOfResultsToRecommend(5);
		
		final SortedSet<DummyRecommendationResult> recommendedTags = merger.getRecommendation(null);

		/*
		 *  check containment and order of top tags
		 */
		final Iterator<DummyRecommendationResult> iterator = recommendedTags.iterator();
		assertEquals("semantic", iterator.next().getTitle());
		assertEquals("social", iterator.next().getTitle());
		assertEquals("nepomuk", iterator.next().getTitle());
		assertEquals("desktop", iterator.next().getTitle());
		assertEquals("project", iterator.next().getTitle());
		assertFalse(iterator.hasNext());
	}


}
