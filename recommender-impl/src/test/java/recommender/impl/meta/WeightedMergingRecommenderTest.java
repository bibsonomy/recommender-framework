/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bibsonomy.recommender.simple.FixedRecommender;
import org.junit.Test;

import recommender.core.Recommender;
import recommender.core.util.RecommendationResultComparator;
import recommender.impl.test.util.DummyRecommendationEntity;
import recommender.impl.test.util.DummyRecommendationResult;


/**
 * @author rja
 */
public class WeightedMergingRecommenderTest {

	@Test
	public void testGetRecommendedTags() {
		final WeightedMergingRecommender<DummyRecommendationEntity, DummyRecommendationResult> recommender = new WeightedMergingRecommender<DummyRecommendationEntity, DummyRecommendationResult>();

		final List<Recommender<DummyRecommendationEntity, DummyRecommendationResult>> tagRecommenders = new ArrayList<Recommender<DummyRecommendationEntity, DummyRecommendationResult>>(2);
		tagRecommenders.add(new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(this.getTags1()));
		tagRecommenders.add(new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(this.getTags2()));
		
		recommender.setRecommenders(tagRecommenders);

		recommender.setWeights(new double[] { 0.4, 0.6 });


		final SortedSet<DummyRecommendationResult> recommendedTags = recommender.getRecommendation(null);

		assertEquals(recommender.getNumberOfResultsToRecommend(), recommendedTags.size());

		/*
		 * for tag 'web': 0.4 * 0.4 + 0.3 * 0.6 = 0.34
		 */
		assertEquals(0.34, recommendedTags.first().getScore(), 0.001);
	}


	private SortedSet<DummyRecommendationResult> getTags1() {
		final SortedSet<DummyRecommendationResult> result = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());

		result.add(new DummyRecommendationResult("semantic",   0.5, 0.1));
		result.add(new DummyRecommendationResult("web",        0.4, 0.1));
		result.add(new DummyRecommendationResult("folksonomy", 0.4, 0.2));
		result.add(new DummyRecommendationResult("holidy",     0.3, 0.5));
		result.add(new DummyRecommendationResult("tree",       0.1, 0.3));

		return result;
	}

	private SortedSet<DummyRecommendationResult> getTags2() {
		final SortedSet<DummyRecommendationResult> result = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());

		result.add(new DummyRecommendationResult("semantic", 0.2, 0.1));
		result.add(new DummyRecommendationResult("web",      0.3, 0.1));
		result.add(new DummyRecommendationResult("car",      0.4, 0.2));
		result.add(new DummyRecommendationResult("holiday",  0.2, 0.5));
		result.add(new DummyRecommendationResult("tree",     0.5, 0.3));

		return result;
	}
}
