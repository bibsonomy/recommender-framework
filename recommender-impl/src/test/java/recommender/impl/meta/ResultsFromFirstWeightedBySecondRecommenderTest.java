/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.SortedSet;

import org.bibsonomy.recommender.simple.FixedRecommender;
import org.junit.Test;

import recommender.impl.test.util.DummyRecommendationEntity;
import recommender.impl.test.util.DummyRecommendationResult;

/**
 * @author rja
 */
public class ResultsFromFirstWeightedBySecondRecommenderTest {

	/**
	 * tests {@link ResultsFromFirstWeightedBySecondRecommender#getRecommendation(recommender.core.interfaces.model.RecommendationEntity)}
	 */
	@Test
	public void testAddRecommendedTags() {
		final String[] firstFixedTags = new String[]{"eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "eins"};
		
		
		final SortedSet<DummyRecommendationResult> firstFixedResults = DummyRecommendationResult.getDummyRecommendationResults(firstFixedTags, 0.5);
		
		// FIXME: 3 times "eins" ?
		final SortedSet<DummyRecommendationResult> secondFixedResults = DummyRecommendationResult.getDummyRecommendationResults(new String[] {"eins", "drei", "vier", "sieben", "eins", "eins", "semantic", "bar", "foo", "net"}, new double[] { 0.3, 0.2, 0.5, 0.6, 0.5, 0.2, 0.5, 0.6, 0.7, 0.8 }, new double[] { 0.2 });
		

		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> first = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(firstFixedResults);
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> second = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(secondFixedResults);
		final ResultsFromFirstWeightedBySecondRecommender<DummyRecommendationEntity, DummyRecommendationResult> merger = new ResultsFromFirstWeightedBySecondRecommender<DummyRecommendationEntity, DummyRecommendationResult>();

		merger.setFirstRecommender(first);
		merger.setSecondRecommender(second);
		merger.setNumberOfResultsToRecommend(5);

		final SortedSet<DummyRecommendationResult> recommendedTags = merger.getRecommendation(null);


		/*
		 *  check containment and order of top tags
		 */
		final Iterator<DummyRecommendationResult> iterator = recommendedTags.iterator();
		assertEquals("sieben", iterator.next().getTitle());
		assertEquals("vier", iterator.next().getTitle());
		assertEquals("eins", iterator.next().getTitle());
		assertEquals("drei", iterator.next().getTitle());
		assertEquals("zwei", iterator.next().getTitle());
		assertFalse(iterator.hasNext());
	}

	/**
	 * tests {@link ResultsFromFirstWeightedBySecondRecommender#getRecommendation(recommender.core.interfaces.model.RecommendationEntity)}
	 * @throws Exception
	 */
	@Test
	public void test2() throws Exception {
		final String[] usersTags = new String[]{"semantic", "web", "social", "net", "graph", "tool", "folksonomy", "holiday"};
		final SortedSet<DummyRecommendationResult> firstFixedResults = DummyRecommendationResult.getDummyRecommendationResults(usersTags, 0.5);
		final SortedSet<DummyRecommendationResult> titleResult = DummyRecommendationResult.getDummyRecommendationResults(new String[]{"nepomuk", "social", "semantic", "desktop"}, 0.5);
		
		final ResultsFromFirstWeightedBySecondRecommender<DummyRecommendationEntity, DummyRecommendationResult> merger = new ResultsFromFirstWeightedBySecondRecommender<DummyRecommendationEntity, DummyRecommendationResult>();
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> simpleContentBasedTagRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(titleResult);
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> fixedTagsTagRecommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(firstFixedResults);

		merger.setFirstRecommender(simpleContentBasedTagRecommender);
		merger.setSecondRecommender(fixedTagsTagRecommender);
		merger.setNumberOfResultsToRecommend(5);


		final SortedSet<DummyRecommendationResult> recommendedTags = merger.getRecommendation(null);

		/*
		 *  check containment and order of top tags
		 */
		final Iterator<DummyRecommendationResult> iterator = recommendedTags.iterator();
		assertEquals("semantic", iterator.next().getTitle());
		assertEquals("social", iterator.next().getTitle());
		assertEquals("nepomuk", iterator.next().getTitle());
		assertEquals("desktop", iterator.next().getTitle());
		assertEquals("web", iterator.next().getTitle());
		assertFalse(iterator.hasNext());
	}

	/**
	 * 
	 * If no tags from first reco are in second reco, we must ensure proper 
	 * scores for fill-up round.
	 * 
	 */
	@Test
	public void testAddRecommendedTags2() {
		final String[] firstFixedTags = new String[]{"eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "eins"};
		final SortedSet<DummyRecommendationResult> firstFixedResults = DummyRecommendationResult.getDummyRecommendationResults(firstFixedTags, 0.5);
		
		final SortedSet<DummyRecommendationResult> secondFixedResults = DummyRecommendationResult.getDummyRecommendationResults(new String[] {"a", "b", "c", "d"}, new double[] { 0.3, 0.2, 0.5, 0.6 }, new double[] { 0.2 });
		
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> first = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(firstFixedResults);
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> second = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(secondFixedResults);
		final ResultsFromFirstWeightedBySecondRecommender<DummyRecommendationEntity, DummyRecommendationResult> merger = new ResultsFromFirstWeightedBySecondRecommender<DummyRecommendationEntity, DummyRecommendationResult>();

		merger.setFirstRecommender(first);
		merger.setSecondRecommender(second);
		merger.setNumberOfResultsToRecommend(5);

		final SortedSet<DummyRecommendationResult> recommendedTags = merger.getRecommendation(null);

		/*
		 *  check containment and order of top tags
		 */
		final Iterator<DummyRecommendationResult> iterator = recommendedTags.iterator();
		final DummyRecommendationResult tag1 = iterator.next();
		final double score = tag1.getScore();
		/*
		 * score should be smaller than 1
		 */
		assertTrue(score < 1.0);
		assertEquals("eins", tag1.getTitle());
		assertEquals("zwei", iterator.next().getTitle());
		assertEquals("drei", iterator.next().getTitle());
		assertEquals("vier", iterator.next().getTitle());
		assertEquals("fünf", iterator.next().getTitle());
		assertFalse(iterator.hasNext());
	}
}
