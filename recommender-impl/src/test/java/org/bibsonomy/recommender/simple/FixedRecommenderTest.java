/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.simple;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Test;

import recommender.core.util.RecommendationResultComparator;
import recommender.impl.test.util.DummyRecommendationEntity;
import recommender.impl.test.util.DummyRecommendationResult;

/**
 * @author rja
 */
public class FixedRecommenderTest {

	/**
	 * Checks, if exactly those tags given in the constructor are returned as recommendation.
	 */
	@Test
	public void testFixedTagsTagRecommenderStringArray() {
		final String[] fixedTags = new String[]{"eins", "zwei", "drei", "vier", "fünf", "sechs", "sieben", "eins"};
		
		final SortedSet<DummyRecommendationResult> fixedResults = DummyRecommendationResult.getDummyRecommendationResults(fixedTags, 0.5);
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> recommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(fixedResults);

		/*
		 * we compare only the names of the tags and disregard their order.
		 */
		final SortedSet<DummyRecommendationResult> recommendedTags = new TreeSet<DummyRecommendationResult>(new Comparator<DummyRecommendationResult>() {
			@Override
			public int compare(final DummyRecommendationResult o1, final DummyRecommendationResult o2) {
				return o1.getTitle().compareTo(o2.getTitle());
			}
		});

		recommender.addRecommendation(recommendedTags, null);

		for (final String tag: fixedTags) {
			assertTrue(recommendedTags.contains(new DummyRecommendationResult(tag, 0.0, 0.0)));
		}
	}

	/**
	 * Checks, if exactly those tags given in the constructor are returned as recommendation.
	 */
	@Test
	public void testFixedTagsTagRecommenderSortedSetOfRecommendedTag() {
		final SortedSet<DummyRecommendationResult> recommendedTags = new TreeSet<DummyRecommendationResult>(new RecommendationResultComparator<DummyRecommendationResult>());
		recommendedTags.add(new DummyRecommendationResult("eins", 0.3, 0.2));
		recommendedTags.add(new DummyRecommendationResult("drei", 0.2, 0.2));
		recommendedTags.add(new DummyRecommendationResult("vier", 0.5, 0.2));
		recommendedTags.add(new DummyRecommendationResult("sieben", 0.6, 0.2));
		recommendedTags.add(new DummyRecommendationResult("eins", 0.5, 0.2));
		recommendedTags.add(new DummyRecommendationResult("eins", 0.2, 0.2));
		recommendedTags.add(new DummyRecommendationResult("semantic", 0.5, 0.2));
		recommendedTags.add(new DummyRecommendationResult("bar", 0.6, 0.2));
		recommendedTags.add(new DummyRecommendationResult("foo", 0.7, 0.2));
		recommendedTags.add(new DummyRecommendationResult("net", 0.8, 0.2));
		final FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult> recommender = new FixedRecommender<DummyRecommendationEntity, DummyRecommendationResult>(recommendedTags);
		assertEquals(recommendedTags, recommender.getRecommendation(null));
	}

}
