/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.util.termprocessing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import org.junit.Test;

/**
 * @author rja
 */
public class TermProcessingIteratorTest {

	@Test
	public void testIt1() {
		final Collection<String> words = Arrays.asList("it","der","Bibsonomy","the","než","\u0438\u0437");
		final Iterator<String> it = new TermProcessingIterator(words.iterator());
		assertTrue(it.hasNext());
		assertEquals("Bibsonomy",it.next());

		assertFalse( it.hasNext() );
	}

	@Test
	public void testIt2() {
		final Collection<String> words = Arrays.asList("it","der","Bibsonomy","the","než","\u0438\u0437", "foo3BaR..");
		final Iterator<String> it = new TermProcessingIterator(words.iterator());
		assertTrue( it.hasNext() );
		assertEquals("Bibsonomy",it.next());
		assertTrue( it.hasNext() );
		assertEquals("foo3BaR..",it.next());
		assertFalse( it.hasNext() );
	}
}
