/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.simple;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.util.RecommendationResultComparator;

/**
 * 
 * @author dzo
 * @param <E> 
 * @param <R> 
 */
public class FixedRecommender<E, R extends RecommendationResult> implements Recommender<E, R> {
	
	protected int numberOfResultsToRecommend = Recommender.DEFAULT_NUMBER_OF_RESULTS_TO_RECOMMEND;
	
	protected SortedSet<R> results = new TreeSet<R>(new RecommendationResultComparator<R>());

	/**
	 * @param results
	 */
	public FixedRecommender(SortedSet<R> results) {
		this.results = results;
	}

	/* (non-Javadoc)
	 * @see recommender.core.Recommender#addRecommendation(java.util.Collection, recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public void addRecommendation(Collection<R> recommendations, E entity) {
		recommendations.addAll(this.results);
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.Recommender#getRecommendation(recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public SortedSet<R> getRecommendation(E entity) {
		return this.results;
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.Recommender#getInfo()
	 */
	@Override
	public String getInfo() {
		return "A simple recommender with a fixed set of results.";
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.Recommender#setFeedback(recommender.core.interfaces.model.RecommendationEntity, recommender.core.interfaces.model.RecommendationResult)
	 */
	@Override
	public void setFeedback(E entity, R result) {
		// ignored
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.Recommender#setNumberOfResultsToRecommend(int)
	 */
	@Override
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend) {
		this.numberOfResultsToRecommend = numberOfResultsToRecommend;
	}
}
