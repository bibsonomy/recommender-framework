/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.util.termprocessing;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Extracts terms from given list of words, using a stopword remover
 * 
 * @author jil
 */
public class TermProcessingIterator implements Iterator<String> {
	private final Iterator<String> words;
	private String next;
	private final StopWordRemover stopwordRemover = StopWordRemover.getInstance();

	/**
	 * @param words
	 */
	public TermProcessingIterator(Iterator<String> words) {
		this.words = words;
		fetchNext();
	}
	
	private void fetchNext() {
		/*
		 * skip empty tags
		 */
		while ((next == null || next.trim().equals("")) && words.hasNext()) {
			/*
			 * clean tag according to challenge rules
			 */
			next = cleanWord(words.next());
			/*
			 * ignore stop words and tags to be ignored according to the challenge rules
			 */
			if (!acceptsWord(next)) {
				next = null;
			}
		}
	}
	
	/**
	 * @param word
	 * @return <code>true</code> if the word is accepted by this iterator
	 */
	protected boolean acceptsWord(final String word) {
		return stopwordRemover.process(word) != null;
	}
	
	/**
	 * @param word
	 * @return the cleanded word
	 */
	protected String cleanWord(String word) {
		return word;
	}

	@Override
	public boolean hasNext() {
		return (next != null);
	}

	@Override
	public String next() {
		final String rVal;
		if (next != null) {
			rVal = next;
			next = null;
			fetchNext();
		} else {
			throw new NoSuchElementException();
		}
		return rVal;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
