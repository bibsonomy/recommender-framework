/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.util.termprocessing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class provides stop word removal for preprocessing.
 * 
 * @author jil
 */
public class StopWordRemover implements TermProcessor {
	private static final Log log = LogFactory.getLog(StopWordRemover.class);
	
	private static final String stopWordFile = "/multilangST.txt";
	
	private static StopWordRemover instance;
	
	/**
	 * @return the {@link StopWordRemover} instance
	 */
	public static StopWordRemover getInstance() {
		if (instance == null) {
			instance = new StopWordRemover();
		}
		return instance;
	}
	
	private final Collection<String> stopWords;
	
	private StopWordRemover() {
		this.stopWords = new HashSet<String>();
		try {
			InputStream is = getClass().getResourceAsStream(stopWordFile);
			if (is == null) {
				throw new IOException("is == null");
			}
			final BufferedReader r = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String sw = null;
			while ((sw = r.readLine()) != null) {
				this.stopWords.add(sw);
			}
			
			r.close();
		} catch (IOException e) {
			log.fatal("Stopwordfile could not be loaded");
			throw new RuntimeException(e);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see recommender.impl.tags.simple.termprocessing.TermProcessor#process(java.lang.String)
	 */
	@Override
	public String process(String term) {
		if (!stopWords.contains(term)) {
			log.debug("not removed word '" + term + "' with length " + term.length());
			return term;
		}
		
		log.debug("removed stopword '" + term + "' with length " + term.length());
		return null;
	}

}
