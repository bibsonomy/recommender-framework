/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.renderer.json.model;

import java.util.SortedSet;

import org.bibsonomy.recommender.renderer.json.model.util.RecommendationResultSortedSet;

import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.model.Status;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * @author dzo
 * @param <E> 
 * @param <R> 
 */
public class RecommendationContainer<E, R extends RecommendationResult> {
	
	private SortedSet<R> recommendations;
	
	private E entity;
	
	private Status status;

	/**
	 * @return the recommendations
	 */
	public SortedSet<R> getRecommendations() {
		return this.recommendations;
	}

	/**
	 * @param recommendations the recommendations to set
	 */
	@JsonDeserialize(as = RecommendationResultSortedSet.class)
	public void setRecommendations(SortedSet<R> recommendations) {
		this.recommendations = recommendations;
	}

	/**
	 * @return the entity
	 */
	public E getEntity() {
		return this.entity;
	}

	/**
	 * @param entity the entity to set
	 */
	public void setEntity(E entity) {
		this.entity = entity;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return this.status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
}
