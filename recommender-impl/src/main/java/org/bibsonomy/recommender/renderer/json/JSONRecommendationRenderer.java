/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.renderer.json;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.SortedSet;

import org.bibsonomy.recommender.renderer.json.model.RecommendationContainer;

import recommender.core.error.BadRequestOrResponseException;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;
import recommender.core.interfaces.renderer.model.Status;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * TODO: add documentation to this class
 *
 * @author dzo
 */
public abstract class JSONRecommendationRenderer<E, R extends RecommendationResult> implements RecommendationRenderer<E, R> {
	
	private final ObjectMapper mapper;
	
	/**
	 * 
	 */
	public JSONRecommendationRenderer() {
		this.mapper = this.createObjectMapper();
	}

	/**
	 * @return
	 */
	protected ObjectMapper createObjectMapper() {
		final ObjectMapper createdMapper = new ObjectMapper();
		createdMapper.setSerializationInclusion(Include.NON_NULL);
		createdMapper.setSerializationInclusion(Include.NON_EMPTY);
		return createdMapper;
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#parseStat(java.io.Reader)
	 */
	@Override
	public String parseStat(Reader reader) throws BadRequestOrResponseException {
		try {
			
			final RecommendationContainer<E, R> container = this.mapper.readValue(reader, createJavaType());
			return container.getStatus().toString();
		} catch (IOException e) {
			// TODO: 
		}
		
		return Status.FAIL.toString();
	}

	protected JavaType createJavaType() {
		return TypeFactory.defaultInstance().constructParametricType(RecommendationContainer.class, this.getEntityClass(), this.getRecommendationResultClass());
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#serializeOK(java.io.Writer)
	 */
	@Override
	public void serializeOK(Writer writer) {
		final RecommendationContainer<E, R> container = new RecommendationContainer<E, R>();
		container.setStatus(Status.OK);
		try {
			this.mapper.writeValue(writer, container);
		} catch (IOException e) {
			// TODO:
		}
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#parseRecommendationEntity(java.io.Reader)
	 */
	@Override
	public E parseRecommendationEntity(Reader reader) throws BadRequestOrResponseException {
		try {
			final RecommendationContainer<E, R> container = this.mapper.readValue(reader, createJavaType());
			return container.getEntity();
		} catch (IOException e) {
			throw new BadRequestOrResponseException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#parseRecommendationResultList(java.io.Reader)
	 */
	@Override
	public SortedSet<R> parseRecommendationResultList(Reader reader) throws BadRequestOrResponseException {
		try {
			final RecommendationContainer<E, R> container = this.mapper.readValue(reader, createJavaType());
			return container.getRecommendations();
		} catch (IOException e) {
			throw new BadRequestOrResponseException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#serializeRecommendationEntity(java.io.Writer, java.lang.Object)
	 */
	@Override
	public void serializeRecommendationEntity(Writer writer, E entity) {
		final RecommendationContainer<E, R> container = new RecommendationContainer<E, R>();
		container.setEntity(entity);
		try {
			this.mapper.writeValue(writer, container);
		} catch (IOException e) {
			// TODO:
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#serializeRecommendationResultList(java.io.Writer, java.util.SortedSet)
	 */
	@Override
	public void serializeRecommendationResultList(Writer writer, SortedSet<R> recommendations) {
		final RecommendationContainer<E, R> container = new RecommendationContainer<E, R>();
		container.setRecommendations(recommendations);
		container.setStatus(Status.OK);
		try {
			this.mapper.writeValue(writer, container);
		} catch (IOException e) {
			// TODO:
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.renderer.RecommendationRenderer#getContentType()
	 */
	@Override
	public String getContentType() {
		return "application/json";
	}
	
	protected abstract Class<?> getEntityClass();
	
	protected abstract Class<?> getRecommendationResultClass();
}
