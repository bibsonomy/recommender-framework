/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.modifiers;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import recommender.core.interfaces.model.RecommendationResult;

/**
 * Result modifier preventing the recommendation of equal results
 * 
 * @author lukas
 *
 * @param <R>
 */
public class RemoveDuplicateResultModifier<R extends RecommendationResult> implements RecommendationResultModifier<R> {

	/*
	 * (non-Javadoc)
	 * @see recommender.impl.modifiers.RecommendationResultModifier#alterResult(java.util.Collection)
	 */
	@Override
	public void alterResult(Collection<R> results) {
		
		Set<String> uniques = new HashSet<String>();
		
		Iterator<R> it = results.iterator();
		while(it.hasNext()) {
			R temp = it.next();
			if(!uniques.contains(temp.getTitle())) {
				uniques.add(temp.getTitle());
			} else {
				it.remove();
			}
		}
		
	}

}
