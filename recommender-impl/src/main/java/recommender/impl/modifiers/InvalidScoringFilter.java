/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.modifiers;

import java.util.Collection;

import recommender.core.interfaces.model.RecommendationResult;

/**
 * Replaces invalid scoring values:
 *    *  NaN : Integer.MIN_VALUE
 *    * -inf : Integer.MIN_VALUE
 *    * +inf : Integer.MAX_VALUE
 * @author fei
 * @param <R> 
 */
public class InvalidScoringFilter<R extends RecommendationResult> implements RecommendationResultModifier<R> {

	@Override
	public void alterResult(Collection<R> resultsToFilter) {
		if (resultsToFilter != null && resultsToFilter.size() > 0) {
			for (final R result : resultsToFilter) {
				double score = result.getScore();
				double confidence = result.getConfidence();
				
				// filter score
				if (Double.isNaN(score)) {
					result.setScore(Integer.MIN_VALUE);
				} else if (score == Double.NEGATIVE_INFINITY) {
					result.setScore(Integer.MIN_VALUE);
				} else if (score == Double.POSITIVE_INFINITY) {
					result.setScore(Integer.MAX_VALUE);
				}

				// filter confidence
				if (Double.isNaN(confidence)) {
					result.setConfidence(Integer.MIN_VALUE);
				} else if (confidence == Double.NEGATIVE_INFINITY) {
					result.setConfidence(Integer.MIN_VALUE);
				} else if (confidence == Double.POSITIVE_INFINITY) {
					result.setConfidence(Integer.MAX_VALUE);
				}
			}
		}
	}
}
