/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.multiplexer;

import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.interfaces.model.RecommendationResult;
/**
 * speeding up the recommender multiplexer by caching recommendations
 *  
 * @author fei
 * @param <E> 
 * @param <R> 
 */
public class RecommendationResultManager<E, R extends RecommendationResult> {
	private static final Log log = LogFactory.getLog(RecommendationResultManager.class);
	
	/** 
	 * we store a list of recommendations for each recommender 
	 * mapping recommender ids to corresponding result sets 
	 */
	ConcurrentHashMap<Long,ConcurrentHashMap<Long, SortedSet<R>>> resultStore;
	/**
	 * All recommenders which finished their process are saved here.
	 * Those are saved even if they did not return valid results.
	 */
	ConcurrentHashMap<Long,ConcurrentLinkedQueue<Long>> finishedRecommender;
	
	/** 
	 * we cache only those results, which are received before timeout -
	 * when a given query timed out, it is flagged by false
	 */
	ConcurrentHashMap<Long, Boolean> monitorFlag;
	
	/**
	 * Constructor
	 */
	public RecommendationResultManager() {
		this.finishedRecommender = new ConcurrentHashMap<Long, ConcurrentLinkedQueue<Long>>();
		this.resultStore = new ConcurrentHashMap<Long, ConcurrentHashMap<Long,SortedSet<R>>>();
		this.monitorFlag = new ConcurrentHashMap<Long, Boolean>();
	}
	
	
	/**
	 * Indicate that given query just started. Results will be added,
	 * until stopQuery(qid) is called
	 * @param qid
	 */
	public void startQuery(Long qid) {
		if( resultStore.containsKey(qid) )
			log.error("Query reinitialized");
		else {
			resultStore.put(qid,new ConcurrentHashMap<Long, SortedSet<R>>());
			finishedRecommender.put(qid, new ConcurrentLinkedQueue<Long>());
			monitorFlag.put(qid, true);
		}
	}
	

	/** 
	 * stop caching results for given query - all further added results to this query
	 * are discarded
	 * @param qid
	 */
	public void stopQuery(Long qid) {
		if(qid == null || !resultStore.containsKey(qid) )
			log.error("Tried to stop non-existant query " + qid);
		else {
			monitorFlag.put(qid, false);
		}
	}
	
	/** 
	 * remove all informations concerning given query
	 * @param qid
	 */
	public void releaseQuery(Long qid) {
		if( !resultStore.containsKey(qid) )
			log.error("Tried to remove non-existant query");
		else {
			finishedRecommender.remove(qid);
			resultStore.remove(qid);
			monitorFlag.remove(qid);
		}
	}
	
	/**
	 * tests whether given query is still monitored
	 */
	private boolean isActive(Long qid) {
		boolean flag1 = resultStore.containsKey(qid);
		boolean flag2 = monitorFlag.containsKey(qid) && monitorFlag.get(qid).booleanValue();
		boolean flag3 = finishedRecommender.containsKey(qid);
		return flag1&&flag2&&flag3;
	}

	/**
	 * tests whether given query is still cached
	 */
	private boolean isCached(Long qid) {
		return resultStore.containsKey(qid);
	}

	
	/**
	 * cache result for given query - if this query is still active
	 * ONLY NON-EMPTY RESULTS ARE STORED
	 * @param qid
	 */
	public void addResult(Long qid, Long sid, SortedSet<R> result) {
		// mark the recommender as finished
		if (isActive(qid)) {
			if (finishedRecommender.keySet().contains(qid)) {
				finishedRecommender.get(qid).add(sid);
			} else {
				log.error("Tried to add finished recommender to non-existant query");
			}
		}
		// add results if present
		if (isActive(qid)) {
			ConcurrentHashMap<Long, SortedSet<R>> queryStore = resultStore.get(qid);
			if( (queryStore!=null) && (result != null && result.size()>0) )
				queryStore.put(sid, result);
		}

	}
	
	/**
	 * Returns all results cached for given query. If the query is not cached, null is 
	 * returned
	 * 
	 * @param qid 
	 * @return
	 */
	public Collection<SortedSet<R>> getResultForQuery(Long qid) {
		if (isCached(qid)) {
			ConcurrentHashMap<Long, SortedSet<R>> queryStore = resultStore.get(qid);
			if (queryStore != null) {
				return queryStore.values();
			}
		}
		return null;
	}
	
	/**
	 * get all recommendations from given recommender in given query, if exists
	 * otherwise null is returned
	 * 
	 * @param qid query id
	 * @param sid recommender's setting id
	 * @return
	 */
	public SortedSet<R> getResults(Long qid, Long sid) {
		if (isCached(qid)) {
			return resultStore.get(qid).get(sid);
		}
		return null;
	}
	
	/**
	 * Returns ids of those recommenders which delivered results for given query - if the query
	 * is cached, otherwise null.
	 * 
	 * @param qid
	 * @return
	 */
	public Set<Long> getActiveRecommender(Long qid) {
		if (isCached(qid)) {
			ConcurrentHashMap<Long, SortedSet<R>> queryStore = resultStore.get(qid);
			if (queryStore != null) {
				return queryStore.keySet();
			}
		}
		return null;
	}
	
	/**
	 * Checks whether a recommender finished for a given query.
	 * The recommender must not have delivered results but his 
	 * algorithms must have successfully pass and addResult had to be called,
	 * regardless of it results were valid.
	 * 
	 * @param sid the recommenders status id
	 * @param qid the query id
	 * @return true if the recommender finished it's algorithm, false if not
	 */
	public boolean isRecommenderFinished(final Long sid, final Long qid) {
		if (finishedRecommender.get(qid) != null) {
			return finishedRecommender.get(qid).contains(sid);
		}
		log.error("Tried to get recommender status for non existant query!");
		return false;
	}
	
	public int getNrOfCachedQueries() {
		return resultStore.size();
	}
}
