/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.multiplexer;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.RecommendationService;
import recommender.core.Recommender;
import recommender.core.database.DBLogic;
import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.filter.PrivacyFilter;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;
import recommender.core.util.RecommendationResultComparator;
import recommender.impl.database.DBLogConfigAccess;
import recommender.impl.dispatcher.FeedbackDispatcher;
import recommender.impl.dispatcher.RecommenderDispatcher;
import recommender.impl.modifiers.EntityModifier;
import recommender.impl.modifiers.RecommendationResultModifier;
import recommender.impl.multiplexer.strategy.RecommendationSelector;
import recommender.impl.multiplexer.strategy.SelectAll;
import recommender.impl.webservice.WebserviceRecommender;

/**
 * Class for querying several recommenders. 
 * Each recommendation request is sent to all registered recommenders. Thereby each 
 * query to a recommender is identified by an unique transaction id ("query id")  which is 
 * used to manage asynchronous events. Responses of the recommenders are collected using 
 * the consumer/producer schema.  
 * The overall result is chosen in selectResult(). Request, all received responses 
 * as well as timing and individual recommender meta information is stored using 
 * class {@link DBLogConfigAccess}.
 * 
 * @author fei
 * @param <E> 
 * @param <R> 
 */
public class MultiplexingRecommender<E, R extends RecommendationResult> implements RecommendationService<E, R>  {
	private static final Log log = LogFactory.getLog(MultiplexingRecommender.class);
	
	/** indicates that entity identifier was not given */
	public static String UNKNOWN_ENTITIYID = "-1";
	
	/** lock for synchronizing concurrent read/write operations */
	Object lockResults = new Object();

	/** timeout for querying distant recommender */
	private int queryTimeout = 100;
	
	/** result selection strategy */
	private RecommendationSelector<E, R> resultSelector;
	
	/** result selector's id (as stored in the db) */
	private long selectorID;

	/** 
	 * not all (especially private entities) should be send to remote recommender
	 * systems - these filter are used for filtering out entities before requests
	 * are sent to remote recommender systems
	 */
	private PrivacyFilter<E> privacyFilter;
	
	/**
	 * these objects may alter certain fields in entity objects before they are
	 * sent to recommender systems (e.g., for making entities anonymous)
	 */
	private List<EntityModifier<E>> entityModifiers;
	
	/** before storing recommendedation results, all these filters are applied */
	private List<RecommendationResultModifier<R>> resultModifiers;
	
	/**
	 * The maximal number of results, the recommender shall return on a call to
	 * {@link #getRecommendation(E)}.
	 */
	private int numberOfResultsToRecommend = Recommender.DEFAULT_NUMBER_OF_RESULTS_TO_RECOMMEND;
	
	/** we speed up the multiplexer by caching query results */
	private final RecommendationResultManager<E, R> resultCache;
	
	/** flag indicating, whether an instance was correctly initialized */
	private boolean initialized = false;
	
	private List<? extends Recommender<E, R>> beanConfiguredRecommenders;
	
	/** map recommender systems to their corresponding setting ids */
	private ConcurrentMap<Long, Recommender<E, R>> activeRecommenders;
	private ConcurrentMap<Long, Recommender<E, R>> availableRecommenders;

	/** class for accessing the recommender internal data base */
	private DBLogic<E, R> dbLogic;
	
	/**
	 * FIXME: maybe there are many multithreaded multiplexer in a running app
	 * debug variable counting the number of open query threads
	 */
	private static int queryThreadCounter = 0;
	
	/**
	 * FIXME: maybe there are many multithreaded multiplexer in a running app
	 * debug variable counting the number of open feedback threads
	 */
	private static int feedbackThreadCounter = 0;
	
	/** renderer for serializing entitites in order to send them to remote recommender services */
	private RecommendationRenderer<E, R> renderer;
	
	/**
	 * constructor.
	 */
	public MultiplexingRecommender() {
		this.resultSelector = new SelectAll<E, R>();
		this.resultCache = new RecommendationResultManager<E, R>();
		this.entityModifiers = new LinkedList<EntityModifier<E>>();
		this.resultModifiers = new LinkedList<RecommendationResultModifier<R>>();
	}
	
	/**
	 * init method: this method has to be called when all 
	 * necessary properties (e.g. dbLogic) are set
	 * 
	 * IMPORTANT: in case of using spring, this init method has to be set in the spring bean definition
	 * <bean id="..." class="..." init-method="init"/>
	 */
	public void init() {
		try {
			this.activeRecommenders = new ConcurrentHashMap<Long, Recommender<E,R>>();
			this.availableRecommenders = new ConcurrentHashMap<Long, Recommender<E,R>>();
			this.registerBeanConfiguredRecommenders();
			this.restoreRemoteRecommenders();
			this.registerResultSelector(this.resultSelector);
			this.initialized = true;
		} catch (final Exception e) {
			// currently catched for the slaves
			log.warn("initializing multiplexing recommender failed");
		}
	}
	
	private void restoreRemoteRecommenders() {
		final List<RecommenderConnector<E, R>> remoteRecommenders = this.dbLogic.getRemoteRecommenders();
		
		for (final RecommenderConnector<E, R> webserviceRecommender : remoteRecommenders) {
			webserviceRecommender.setRecommendationRenderer(this.renderer);
			activateRecommenderIfActive(webserviceRecommender);
		}
	}

	private void registerBeanConfiguredRecommenders() {
		for (final Recommender<E, R> recommender : beanConfiguredRecommenders) {
			// handle cold start problem
			if (!this.dbLogic.isRecommenderRegistered(recommender)) {
				this.dbLogic.registerRecommender(recommender);
			}
			
			activateRecommenderIfActive(recommender);
		}
	}

	private void activateRecommenderIfActive(Recommender<E, R> recommender) {
		final Long recommenderId = this.dbLogic.getRecommenderId(recommender);
		if (this.dbLogic.isRecommenderActive(recommender)) {
			this.activeRecommenders.put(recommenderId, recommender);
		}
		
		recommender.setNumberOfResultsToRecommend(this.numberOfResultsToRecommend);
		this.availableRecommenders.put(recommenderId, recommender);
	}
	
	/**
	 * adds a recommender to the multiplexer
	 * @param recommender
	 */
	public void addRecommender(final Recommender<E, R> recommender) {
		this.dbLogic.registerRecommender(recommender);
		
		final Long recommenderId = this.dbLogic.getRecommenderId(recommender);
		
		if (recommender instanceof WebserviceRecommender<?, ?>) {
			final WebserviceRecommender<E, R> connector = (WebserviceRecommender<E, R>) recommender;
			connector.setRecommendationRenderer(this.renderer);
		}
		
		this.availableRecommenders.put(recommenderId, recommender);
		this.activeRecommenders.put(recommenderId, recommender);
	}
	

	/**
	 * @param recommenderId
	 * @return <code>true</code> iff the recommender was removed from the multiplexer
	 */
	public boolean removeRecommender(Long recommenderId) {
		final Recommender<E, R> recommender = this.availableRecommenders.remove(recommenderId);
		if (recommender != null) {
			this.availableRecommenders.remove(recommenderId);
			this.dbLogic.removeRecommender(recommender);
			return true;
		}
		return false;
	}
	
	/**
	 * Enable/activate a recommender identified by its settingid and regardless of its type (distant or local).
	 * @param sid
	 * @return true on success
	 */
	public boolean enableRecommender(final Long sid) {
		if (sid == null) {
			return false;
		}
		
		if (this.activeRecommenders.containsKey(sid)) {
			log.debug("recommender with id " + sid + " already active.");
			return false;
		}
		
		if (!this.availableRecommenders.containsKey(sid)) {
			log.warn("recommender with id " + sid + " not found.");
			return false;
		}
		
		final Recommender<E, R> recommender = this.availableRecommenders.get(sid);
		this.dbLogic.updateRecommenderstatus(Collections.singletonList(sid), null);
		
		this.activeRecommenders.put(sid, recommender);
		return true;
	}
	
	
	/** 
	 *  Disable/deactivate a recommender (distant or local).
	 *  @param sid SettingId
	 *  @return true if this recommender was activated (and is now deactivated)
	 *  */ 
	public boolean disableRecommender(final Long sid) {
		if ((sid == null) || !this.activeRecommenders.containsKey(sid)) {
			return false; // No recommender with this settingId
		}
		
		if (this.activeRecommenders.size() == 1) {
			return false;
		}
		
		final Recommender<E, R> removed = this.activeRecommenders.remove(sid);
		if (removed != null) {
			this.dbLogic.updateRecommenderstatus(null, Collections.singletonList(sid));
			return true;
		}
		
		return false;
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.RecommendationService#getRecommendationsForUser(java.lang.String, recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public SortedSet<R> getRecommendationsForUser(String userName, E entity) {
		final SortedSet<R> recommendationResults = new TreeSet<R>(new RecommendationResultComparator<R>());
		log.debug("[" + entity + "] querying " + this.activeRecommenders);

		// list for managing pending recommenders
		final List<RecommenderDispatcher<E, R>> dispatchers = new LinkedList<RecommenderDispatcher<E, R>>();
		
		// query's time stamp
		final Date ts = new Date();
		
		// get a new query id for identifying the recommendation
		final Long qid = this.dbLogic.addQuery(userName, ts, entity, this.queryTimeout);
		
		// add query to cache
		this.resultCache.startQuery(qid);
		
		/*
		 * query remote recommender systems - we filter out certain entities for respecting privacy
		 */
		for (final Entry<Long, Recommender<E, R>> activeRecommenderEntry : this.activeRecommenders.entrySet()) {
			final Recommender<E, R> recommender = activeRecommenderEntry.getValue();
			final Long id = activeRecommenderEntry.getKey();
			final E entityToUse = this.modifyEntity(entity, recommender);
			if (entityToUse == null) {
				continue;
			}
			
			this.dbLogic.addRecommenderToQuery(qid, id);
			final RecommenderDispatcher<E, R> dispatcher = new RecommenderDispatcher<E, R>(recommender, entityToUse, qid, id, null, this);
			dispatchers.add(dispatcher);
			dispatcher.start();
		}
		
		// wait for recommender systems' answers
		final long startSleep = System.currentTimeMillis();
		try {
			Thread.sleep(this.queryTimeout); 
		} catch (final InterruptedException e) {
			log.debug("Sleep was interrupted");
		}
		// stop monitoring this query in the result cache
		this.resultCache.stopQuery(qid);

		// tell dispatchers that they are late
		for (final RecommenderDispatcher<E, R> disp : dispatchers) {
			disp.abortQuery();
		}
		
		log.debug("(" + qid + ") Waited for " + (System.currentTimeMillis() - startSleep) + " ms");
		
		if (qid != null) {
			this.selectResult(qid, recommendationResults);
		}
		
		log.debug("(" + qid + ") Running threads: " + queryThreadCounter + " query threads and " + feedbackThreadCounter + " feedback threads");
		return recommendationResults;
	}
	
	/**
	 * sets feedback for recommendation on entity
	 */
	@Override
	public void setFeedback(String userName, final E entity, final R result) {
		this.dbLogic.addFeedback(userName, entity, result);
		
		/*
		 * notify the recommenders about the actual chosen values
		 */
		// list for managing pending recommenders
		final List<FeedbackDispatcher<E, R>> dispatchers = new LinkedList<FeedbackDispatcher<E, R>>();
		
		for (final Recommender<E, R> recommender : this.activeRecommenders.values()) {
			final E entityToUse = modifyEntity(entity, recommender);
			if (entityToUse == null) {
				continue;
			}
			
			final FeedbackDispatcher<E, R> dispatcher = new FeedbackDispatcher<E, R>(recommender, entity, result);
			dispatchers.add(dispatcher);
			dispatcher.start();
		}
	}
	
	private E modifyEntity(E entity, final Recommender<E, R> recommender) {
		if (recommender instanceof RecommenderConnector<?, ?>) {
			final RecommenderConnector<E, R> connector = (RecommenderConnector<E, R>) recommender;
			if (!connector.isTrusted()) {
				entity = this.privacyFilter.filterEntity(entity);
				for (final EntityModifier<E> pm : this.entityModifiers) {
					pm.alterEntity(entity);
				}
			}
		}
		
		return entity;
	}
	
	/**
	 * After querying all recommenders, the final result is composed here.
	 * @throws SQLException
	 */
	private void selectResult(final Long queryId, final Collection<R> recommendationResults) {
		log.debug("(" + queryId + ") starting result selection");
		
		// select result
		this.resultSelector.selectResult(queryId, this.resultCache, recommendationResults);
		this.dbLogic.storeRecommendation(queryId, Long.valueOf(this.selectorID), recommendationResults);

		// trim number of recommendations if it exceeds numberOfResultsToRecommend
		if (recommendationResults.size() > this.numberOfResultsToRecommend) {
			final Iterator<R> itr = recommendationResults.iterator();
			int pos = 0;
			while (itr.hasNext()) {
				itr.next();
				pos++;
				if (pos > this.numberOfResultsToRecommend) {
					itr.remove();
				}
			}
		}
		
		// remove query from result cache
		this.resultCache.releaseQuery(queryId);
		log.debug("(" + queryId + ") Released query from result cache (" + this.resultCache.getNrOfCachedQueries() + " remaining).");
	}
	
	/**
	 * Publish individual (asynchronous) recommender's response.
	 * @param queryId
	 * @param recommenderId unique id identifying recommender
	 * @param queryTime time from call to recommender's response 
	 * @param results recommender's result. If null, recommender timed out. 
	 * @return true on success, false otherwise
	 * @throws SQLException 
	 */
	public synchronized boolean addQueryResponse(final Long queryId, final Long recommenderId, final long queryTime, final SortedSet<R> results) {
		if (results != null && results.size() > 0) {
			// filter out invalid recommendations
			for (final RecommendationResultModifier<R> filter : this.resultModifiers) {
				filter.alterResult(results);
			}
		}
		// put result to resultCache (if query is still active)
		if (queryTime <= this.queryTimeout) {
			this.resultCache.addResult(queryId, recommenderId, results);
		}
		
		// store result in the database
		this.dbLogic.addRecommendation(queryId, recommenderId, results, queryTime);
		
		return true;
	}
	
	/**
	 * increase the query thread counter
	 */
	public static synchronized void incQueryCounter() {
		MultiplexingRecommender.queryThreadCounter++;
	}
	
	/**
	 * decrease the query thread counter
	 */
	public static synchronized void decQueryCounter() {
		MultiplexingRecommender.queryThreadCounter--;
	}
	
	/**
	 * increase the feedback thread counter
	 */
	public static synchronized void incFeedbackCounter() {
		MultiplexingRecommender.feedbackThreadCounter++;
	}
	
	/**
	 * decrease the feedback thread counter
	 */
	public static synchronized void decFeedbackCounter() {
		MultiplexingRecommender.feedbackThreadCounter--;
	}
	
	/**
	 * register result selection strategy
	 * 
	 * @param selector the selection strategy
	 */
	private void registerResultSelector(final RecommendationSelector<E, R> selector) {
		this.selectorID = this.dbLogic.insertSelectorSetting(this.resultSelector.getInfo(), this.resultSelector.getMeta()).longValue();
	}
	
	/**
	 * @param queryTimeout the timeout after which a result should invalidate
	 */
	public void setQueryTimeout(final int queryTimeout) {
		this.queryTimeout = queryTimeout;
	}

	/**
	 * @param resultSelector the selector which selects the final results
	 */
	public void setResultSelector(final RecommendationSelector<E, R> resultSelector) {
		this.resultSelector = resultSelector;
		if (this.initialized) {
			this.registerResultSelector(resultSelector);
		}
	}
	
	/**
	 * @param renderer a renderer for serialisation and deserialization of {@link #E} and {@link #R}
	 */
	public void setRenderer(RecommendationRenderer<E, R> renderer) {
		this.renderer = renderer;
	}

	/**
	 * @param privacyFilter the privacyFilter for respecting privacy settings to set
	 */
	public void setPrivacyFilter(PrivacyFilter<E> privacyFilter) {
		this.privacyFilter = privacyFilter;
	}
	
	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#setNumberOfResultsToRecommend(int)
	 */
	@Override
	public void setNumberOfResultsToRecommend(final int numberOfResultsToRecommend) {
		this.numberOfResultsToRecommend = numberOfResultsToRecommend;
	}
	
	/**
	 * @param dbLogic an implementation of the configuration and logging database logic
	 */
	public void setDbLogic(final DBLogic<E, R> dbLogic) {
		this.dbLogic = dbLogic;
	}

	/**
	 * @param entityModifiers a list of modifiers which are applied to the {@link #E}s, before
	 * they are send to a remote recommender
	 */
	public void setEntityModifiers(final List<EntityModifier<E>> entityModifiers) {
		this.entityModifiers = entityModifiers;
	}
	
	/**
	 * @param resultModifiers a list of modifiers which are applied to the results of each recommender
	 */
	public void setResultModifiers(final List<RecommendationResultModifier<R>> resultModifiers) {
		this.resultModifiers = resultModifiers;
	}
	
	/**
	 * @param beanConfiguredRecommenders the beanConfiguredRecommenders to set
	 */
	public void setBeanConfiguredRecommenders(List<? extends Recommender<E, R>> beanConfiguredRecommenders) {
		this.beanConfiguredRecommenders = beanConfiguredRecommenders;
	}

	/**
	 * @return all recommenders
	 */
	public Collection<Recommender<E, R>> getAllRecommenders() {
		return this.availableRecommenders.values();
	}
	
	/**
	 * @return all active recommenders
	 */
	public Collection<Recommender<E, R>> getAllActiveRecommenders() {
		return this.activeRecommenders.values();
	}
}