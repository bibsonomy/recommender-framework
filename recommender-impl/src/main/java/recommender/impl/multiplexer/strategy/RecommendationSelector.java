/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.multiplexer.strategy;

import java.util.Collection;

import recommender.core.interfaces.model.RecommendationResult;
import recommender.impl.multiplexer.RecommendationResultManager;

/**
 * Interface for recommendation selection out of different recommenders.
 * 
 * @author fei
 * @param <E> 
 * @param <R> 
 */
public interface RecommendationSelector<E, R extends RecommendationResult> {
	
	/**
	 * Selects recommendations for given query
	 * 
	 * @param qid
	 * @param resultCache 
	 * @param recommendationResults 
	 */
	public void selectResult(Long qid, RecommendationResultManager<E, R> resultCache, Collection<R> recommendationResults);
	
	/**
	 * selector specific meta informations
	 * @param info 
	 */
	public void setInfo(String info);
	
	/**
	 * @return selector specific meta informations
	 */
	public String getInfo();
	
	/**
	 * short text describing this strategy
	 * @param meta 
	 */
	public void setMeta(byte[] meta);
	
	/**
	 * @return short text describing this strategy
	 */
	public byte[] getMeta();
}
