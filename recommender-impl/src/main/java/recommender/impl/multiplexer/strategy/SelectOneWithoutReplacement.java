/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.multiplexer.strategy;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.model.Pair;
import recommender.impl.multiplexer.RecommendationResultManager;

/**
 * This selection strategy selects exactly one recommender.
 *  
 * @author fei
 * @param <E> 
 * @param <R> 
 */
public class SelectOneWithoutReplacement<E, R extends RecommendationResult> extends SimpleSelector<E, R> {
	private static final Log log = LogFactory.getLog(SelectOneWithoutReplacement.class);
	
	/**
	 * Selection strategy which selects recommender (uniform) randomly.
	 * If selected recommender didn't deliver recommendations - a fallback
	 * recommender is chosen. 
	 */
	@Override
	public void selectResult(final Long qid, final RecommendationResultManager<E, R> resultCache, final Collection<R> recommendationResults) {
		log.debug("(" + qid + ")Selecting result.");
		
		// get list of recommenders which delivered results in given query
		final SortedSet<Long> listActive = new TreeSet<Long>(resultCache.getActiveRecommender(qid));
		

		log.debug("(" + qid + ")Selecting result #1");
		// get list of all recommenders for this recommendation process with corresponding number of 
		// queries where they were selected
		final List<Pair<Long,Long>> selectionCount = dbLogic.getRecommenderSelectionCount(qid);
		
		/*
		 * create list of all recommenders from which the next one shall be drawn
		 */
		final Vector<Long> listAll = new Vector<Long>();
		
		// id of last recommender
		long last = -1;
		if (!selectionCount.isEmpty()) {
			last = selectionCount.get(0).getSecond();
		}
			
		// collect those recommenders which were selected least 
		log.debug("(" + qid + ")Selecting result #2");
		while (!selectionCount.isEmpty() && (selectionCount.get(0).getSecond() == last)) {
			listAll.add(selectionCount.get(0).getFirst());
			selectionCount.remove(0);
		}
		log.debug("(" + qid + ")Selecting result #3");

		// if no recommendation available, append nothing
		if (listAll.size() == 0 || listActive.size() == 0) {
			log.debug("(" + qid + ")No results available!");
			return;
		}
		
		// select recommender
		Long sid = listAll.get((int) Math.floor((Math.random() * listAll.size())));
		
		// store selection in database
		dbLogic.addSelectedRecommender(qid, sid);
		log.debug("(" + qid + ")Selected setting " + sid + " out of " + listActive.size() + "/" + listAll.size());
		
		// check if selected recommender delivered recommendations
		boolean isActive = false;
		for (final Long i : listActive) {
			if (i.equals(sid)) { 
				isActive = true;
			}
		}
		// if not, select a fall back recommender
		if (!isActive) {
			sid = this.getFromSet(listActive, (int) Math.floor((Math.random()*listActive.size())));
			log.debug("(" + qid + ")Selected setting not active, fall back is " + sid);
		}
		
		// finally get recommendations
		final SortedSet<R> cachedResult = resultCache.getResults(qid,sid);
		if (cachedResult != null) {
			recommendationResults.addAll(cachedResult);
		} else {
			// this shouldn't happen!
			log.error("(" + qid + ")Selected result not cached -> fetching it from database");

			dbLogic.getRecommendations(qid, sid, recommendationResults);

		}
	}	
	
	/**
	 * helper method returns the element in specified position from a sorted set
	 * 
	 * @param set the set to retrieve the object from
	 * @param position the position in the set
	 * @return the object in the specified position or the minimum long value if the position was not valid
	 */
	private Long getFromSet(final SortedSet<Long> set, int position) {
		if (position < 0) {
			return Long.MIN_VALUE;
		}
		for (Long sid : set) {
			position--;
			if (position < 0) {
				return sid;
			}
		}
		
		return Long.MIN_VALUE;
	}

	@Override
	public String getInfo() {
		return "Strategy for selecting one recommender.";
	}
}
