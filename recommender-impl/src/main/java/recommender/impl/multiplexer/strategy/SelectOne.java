/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.multiplexer.strategy;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.interfaces.model.RecommendationResult;
import recommender.impl.multiplexer.RecommendationResultManager;

/**
 * This selection strategy selects exactly one recommender.
 *  
 * @author fei
 * @param <E> 
 * @param <R> 
 */
public class SelectOne<E, R extends RecommendationResult> extends SimpleSelector<E, R> {
	private static final Log log = LogFactory.getLog(SelectOne.class);

	/**
	 * Selection strategy which selects recommender (uniform) randomly.
	 * If selected recommender didn't deliver recommendations - a fallback
	 * recommender is chosen. 
	 */
	@Override
	public void selectResult(final Long qid, final RecommendationResultManager<E, R> resultCache, final Collection<R> recommendationsResults) {
		log.debug("Selecting result.");
		
		// get list of recommenders which delivered recommendations in given query
		final List<Long> listActive = dbLogic.getActiveRecommenderIDs(qid);
		// get list of all recommenders for given query
		final List<Long> listAll = dbLogic.getAllRecommenderIDs(qid);
		
		
		// if no recommendation available, append nothing
		if (listAll.size() == 0 || listActive.size() == 0) {
			log.warn("No results available!");
			return;
		}
		
		// select recommender
		Long sid = listAll.get((int) Math.floor((Math.random() * listAll.size())));
		// store selection in database
		dbLogic.addSelectedRecommender(qid, sid);
		log.debug("Selected setting " + sid + " out of " + listActive.size() + "/" + listAll.size());
		
		// check if selected recommender delivered results
		boolean isActive = false;
		for (final Iterator<Long> i = listActive.iterator(); i.hasNext(); ) {
			final Long next = i.next();
			if( next.equals(sid) ) 
				isActive = true;
		}
		// if not, select a fall back recommender
		if (!isActive) {
			sid = listActive.get((int) Math.floor((Math.random() * listActive.size())));
			log.debug("Selected setting not active, fall back is " + sid);
		}
		
		// finally get recommendations
		final SortedSet<R> cachedResult = resultCache.getResults(qid,sid);
		if (cachedResult != null) {
			recommendationsResults.addAll(cachedResult);
		} else {
			// this shouldn't happen!
			log.error("(" + qid + ") Selected result not cached -> fetching it from database");
			dbLogic.getRecommendations(qid, sid, recommendationsResults);
		}
	}

	@Override
	public String getInfo() {
		return "Strategy for selecting one recommender.";
	}
}
