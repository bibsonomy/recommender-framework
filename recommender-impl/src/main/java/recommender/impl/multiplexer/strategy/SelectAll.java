/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.multiplexer.strategy;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.model.Pair;
import recommender.impl.multiplexer.RecommendationResultManager;

/**
 * @author fei
 * @param <E> 
 * @param <R> 
 */
public class SelectAll<E, R extends RecommendationResult> extends SimpleSelector<E, R> {
	private static final Log log = LogFactory.getLog(SelectAll.class);
	
	/**
	 * Selection strategy which simply selects each recommendation result
	 */
	@Override
	public void selectResult(final Long qid, final RecommendationResultManager<E, R> resultCache, final Collection<R> recommendationResults) {
		log.debug("Selecting result.");
		
		final List<Pair<Long,Long>> selectionCount = dbLogic.getRecommenderSelectionCount(qid);
		
		for (Pair<Long, Long> selection : selectionCount) {
			SortedSet<R> result = resultCache.getResults(qid, selection.getFirst());
			if (result != null && result.size() > 0) {
				recommendationResults.addAll(result);
			} else if (resultCache.isRecommenderFinished(selection.getFirst(), qid)) {
				log.error("(" + qid + ")Selected result not cached -> fetching it from database");
				
				recommendationResults.addAll(this.dbLogic.getRecommendations(qid, selection.getFirst()));
			}
		}
		
		log.debug("all recommendations added.");
	}

	@Override
	public String getInfo() {
		return "Strategy for selecting all recommendation results.";
	}
}
