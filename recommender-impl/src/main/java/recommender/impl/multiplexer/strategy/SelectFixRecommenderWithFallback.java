/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.multiplexer.strategy;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.impl.multiplexer.RecommendationResultManager;

/**
 * Selects a recommender of a fixed specified type.
 * If the recommender of the primary type did not deliver results, a
 * recommender of the fallback type is chosen.
 * 
 * @author lukas
 *
 * @param <E>
 * @param <R>
 */
public class SelectFixRecommenderWithFallback<E, R extends RecommendationResult> extends SimpleSelector<E, R> {
	private static final Log log = LogFactory.getLog(SelectFixRecommenderWithFallback.class);

	private Recommender<E, R> primaryRecommender;
	private Recommender<E, R> fallbackRecommender;
	
	/**
	 * Selection strategy which selects recommender by primary type.
	 * If selected recommender didn't deliver recommendations a fallback
	 * recommender is chosen. 
	 */
	@Override
	public void selectResult(final Long qid, final RecommendationResultManager<E, R> resultCache, final Collection<R> recommendationResults) {
		log.debug("Selecting result.");
		
		// get list of recommenders which delivered results in given query
		final List<Long> listActive = dbLogic.getActiveRecommenderIDs(qid);
		// get list of all recommenders for given query
		final List<Long> listAll = dbLogic.getAllRecommenderIDs(qid);
		
		// get sid of primary recommender
		final Long primarySid = this.dbLogic.getRecommenderId(primaryRecommender);
		
		// get sid of fallbackrecommender
		final Long fallbackSid = this.dbLogic.getRecommenderId(fallbackRecommender);
		
		Long chosenSid = primarySid;
		if (primarySid.longValue() == -1L) {
			log.debug("Primary recommender is not registered!");
			chosenSid = fallbackSid;
		}
		if (fallbackSid.longValue() == -1L) {
			log.debug("Fallback recommender is not registered!");
		}
		
		// if no recommendation available, append nothing
		if (listAll.size() == 0 || listActive.size() == 0) {
			log.debug("No results available!");
			return;
		}
		
		if (!listActive.contains(primarySid) && !listActive.contains(fallbackSid)) {
			log.debug("Neither primary nor fallback recommender delivered results!");
			return;
		}
		
		// in case of only fallback delivered results take fallback recommendations
		if (!listActive.contains(primarySid)) {
			chosenSid = fallbackSid;
		}
		
		// store selection in database
		dbLogic.addSelectedRecommender(qid, chosenSid);
		if (chosenSid == primarySid) {
			log.debug("Selected setting " + chosenSid + " as primary recommender!");
		} else {
			log.debug("Selected setting " + chosenSid + " as fallback recommender!");
		}
		
		// finally get recommendations
		final SortedSet<R> cachedResult = resultCache.getResults(qid, chosenSid);
		if (cachedResult != null) {
			recommendationResults.addAll(cachedResult);
		} else {
			// this shouldn't happen!
			log.error("(" + qid + ") Selected result not cached -> fetching it from database");
			dbLogic.getRecommendations(qid, chosenSid, recommendationResults);
		}
	}

	@Override
	public String getInfo() {
		return "Select fixed recommender or fallback.";
	}

	/**
	 * @param primaryRecommender the primaryRecommender to set
	 */
	public void setPrimaryRecommender(Recommender<E, R> primaryRecommender) {
		this.primaryRecommender = primaryRecommender;
	}

	/**
	 * @param fallbackRecommender the fallbackRecommender to set
	 */
	public void setFallbackRecommender(Recommender<E, R> fallbackRecommender) {
		this.fallbackRecommender = fallbackRecommender;
	}
}
