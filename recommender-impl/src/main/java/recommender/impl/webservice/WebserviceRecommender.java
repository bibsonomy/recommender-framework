/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.webservice;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.DisposableBean;

import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;
import recommender.core.util.RecommendationResultComparator;

/**
 * Class for encapsulating webservice queries to recommenders
 * 
 * @author fei
 * 
 * @param <E> the recommendation entity
 * @param <R> the recommendation
 */
public class WebserviceRecommender<E, R extends RecommendationResult> implements RecommenderConnector<E, R>, DisposableBean {
	private static final Log log = LogFactory.getLog(WebserviceRecommender.class);
	
	private static final int SOCKET_TIMEOUT_MS = 10000;
	private static final int HTTP_CONNECTION_TIMEOUT_MS = 1000;
	private static final long IDLE_TIMEOUT_MS = 3000;
	
	private boolean trusted;
	private final CloseableHttpClient client;
	// service's address
	private URL address;
	// serializes entity
	private RecommendationRenderer<E, R> renderer;
	
	// ConnectionManager 
	private final PoolingHttpClientConnectionManager connectionManager;
	
	/**
	 * default constructor
	 */
	public WebserviceRecommender() {
		// create and config connection manager
		this.connectionManager = new PoolingHttpClientConnectionManager();
		this.connectionManager.closeIdleConnections(IDLE_TIMEOUT_MS, TimeUnit.MILLISECONDS);

		// create an instance of HttpClient.
		final RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();
		requestConfigBuilder.setSocketTimeout(SOCKET_TIMEOUT_MS);
		requestConfigBuilder.setConnectionRequestTimeout(HTTP_CONNECTION_TIMEOUT_MS);
		requestConfigBuilder.setConnectTimeout(HTTP_CONNECTION_TIMEOUT_MS);
		final HttpClientBuilder builder = HttpClientBuilder.create();
		builder.setDefaultRequestConfig(requestConfigBuilder.build());
		builder.setConnectionManager(this.connectionManager);
		this.client = builder.build();
	}
	
	/**
	 * inits the recommender
	 * @param renderer 
	 */
	public WebserviceRecommender(final RecommendationRenderer<E, R> renderer) {
		this();
		this.renderer = renderer;
	}

	/**
	 * Constructor
	 * @param renderer
	 * @param address
	 */
	public WebserviceRecommender(final RecommendationRenderer<E, R> renderer, final URL address) {
		this(renderer);
		this.setAddress(address);
	}
	
	/**
	 * @return the address
	 */
	public URL getAddress() {
		return this.address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(URL address) {
		this.address = address;
	}
	
	/**
	 * @param renderer the renderer to serialize recommendation entities and recommendation results
	 */
	@Override
	public void setRecommendationRenderer(RecommendationRenderer<E, R> renderer) {
		this.renderer = renderer;
	}
	
	@Override
	public void addRecommendation(final Collection<R> recommendationResults, final E entity) {
		// render entity
		// FIXME: choose buffer size
		final StringWriter sw = new StringWriter(100);
		renderEntity(entity, sw);

		// Create a method instance and send request
		final HttpPost cnct = new HttpPost(getAddress().toString() + "/" + METHOD_GETRECOMMENDEDTAGS);
		final UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity(Collections.singletonList(new BasicNameValuePair(ID_RECQUERY, sw.toString())), Charset.forName("UTF-8"));
		cnct.setEntity(requestEntity);

		final InputStreamReader input = sendRequest(cnct);
		
		// Deal with the response.
		SortedSet<R> result = null;
		if (input != null) {
			try {
				result = renderer.parseRecommendationResultList(input);
			} catch (final Exception e) {
				log.error("Error parsing recommender response (" + getAddress().toString() + ").", e);
				result = null;
			} finally {
				try {
					input.close();
				} catch (IOException e) {
					log.error("error while closing input stream", e);
				}
			}
		}
		if (result != null) {
			recommendationResults.addAll(result);
		}
		cnct.releaseConnection();
	}
	
	@Override
	public SortedSet<R> getRecommendation(final E entity) {
		final SortedSet<R> retVal = new TreeSet<>(new RecommendationResultComparator<R>());
		addRecommendation(retVal, entity);
		return retVal;
	}
	
	@Override
	public void setFeedback(final E entity, final R result) {
		// render entity
		final StringWriter sw = new StringWriter(100);
		renderEntity(entity, sw);

		// send request
		final HttpPost cnct = new HttpPost(getAddress().toString() + "/" + METHOD_SETFEEDBACK);
		final UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity(Collections.singletonList(new BasicNameValuePair(ID_RECQUERY, sw.toString())), Charset.forName("UTF-8"));
		cnct.setEntity(requestEntity);

		final InputStreamReader input = sendRequest(cnct);

		// Deal with the response.
		if (input != null) {
			// TODO: check for response code before parsing the state
			final String status = renderer.parseStat(input);
			log.info("Feedback status: " + status);
			
			try {
				input.close();
			} catch (IOException e) {
				log.error("error while closing connection ", e);
			}
		}
		
		cnct.releaseConnection();
	}

	@Override
	public String getInfo() {
		return "Webservice";
	}

	@Override
	public String getId() {
		return getAddress().toString();
	}
	
	@Override
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend) {
		// nothing to do
	}
	
	/* (non-Javadoc)
	 * @see recommender.core.interfaces.RecommenderConnector#getRecommendationRenderer()
	 */
	@Override
	public RecommendationRenderer<E, R> getRecommendationRenderer() {
		return this.renderer;
	}

	private void renderEntity(final E entity, final StringWriter sw) {
		renderer.serializeRecommendationEntity(sw, entity);
	}

	private InputStreamReader sendRequest(final HttpPost cnct) {
		// add some header
		cnct.setHeader(HttpHeaders.CONNECTION, "close");

		try {
			// Execute the method.
			try (final CloseableHttpResponse response = this.client.execute(cnct)) {
				final StatusLine statusLine = response.getStatusLine();
				final int statusCode = statusLine.getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					log.error("Method at " + getAddress().toString() + " failed: " + statusLine.getReasonPhrase());
				} else {
					// Read the response body.
					final HttpEntity responseEntity = response.getEntity();
					return new InputStreamReader(responseEntity.getContent(), responseEntity.getContentEncoding().getValue());
				}
			}
		} catch (final UnsupportedEncodingException ex) {
			// returns InputStream with default encoding if a exception
			// is thrown with utf-8 support
			log.fatal("Encoding error(" + this.address + "): " + ex.getMessage(), ex);
		} catch (final IOException e) {
			log.fatal("Fatal transport error(" + this.address + "): " + e.getMessage(), e);
		} catch (final Exception e) {
			log.fatal("Unknown error (" + this.address + ")", e);
		}
		
		// all done.
		return null;
	}

	@Override
	public void destroy() throws Exception {
		// needed to prevent a failing spring-context to start more and more threads
		if (this.connectionManager != null) {
			this.connectionManager.shutdown();
		}
	}

	/**
	 * @return the trusted
	 */
	@Override
	public boolean isTrusted() {
		return this.trusted;
	}

	/**
	 * @param trusted the trusted to set
	 */
	public void setTrusted(boolean trusted) {
		this.trusted = trusted;
	}
}
