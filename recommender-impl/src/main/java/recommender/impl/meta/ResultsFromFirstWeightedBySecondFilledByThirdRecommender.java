/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import java.util.Collection;
import java.util.SortedSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;

/**
 * Takes the results from {@link #firstRecommender} and orders them by their scores
 * from {@link #secondRecommender}. If they're not recommended by {@link #secondRecommender},
 * they get a score of 0. If {@link #firstRecommender} can't deliver enough results, they're filled
 * up by the top results from {@link #thirdRecommender}.
 * 
 * @author rja
 * 
 * @param <E> the recommendation entity
 * @param <R> the recommendation
 */
public class ResultsFromFirstWeightedBySecondFilledByThirdRecommender<E, R extends RecommendationResult> extends ResultsFromFirstWeightedBySecondRecommender<E, R> {
	private static final Log log = LogFactory.getLog(ResultsFromFirstWeightedBySecondFilledByThirdRecommender.class);

	private Recommender<E, R> thirdRecommender;

	/**
	 * Initializes the recommender with the given recommenders.
	 * 
	 * @param firstRecommender - delivers the main recommendations, which are scored by the secondRecommender
	 * @param secondRecommender - used to score the results from the first recommender
	 * @param thirdRecommender - if the first recommender does not provide enough recommendations, this recommender can fill them up
	 */
	public ResultsFromFirstWeightedBySecondFilledByThirdRecommender(Recommender<E, R> firstRecommender, Recommender<E, R> secondRecommender, Recommender<E, R> thirdRecommender) {
		super(firstRecommender, secondRecommender);
		this.thirdRecommender = thirdRecommender;
	}

	/**
	 * Don't initializes any recommenders - you have to call the setters! 
	 */
	public ResultsFromFirstWeightedBySecondFilledByThirdRecommender() {
		super(); 
	}

	@Override
	protected void addRecommendationInternal(final Collection<R> recommendationResults, final E entity) {
		if (firstRecommender == null || secondRecommender == null || thirdRecommender == null) {
			throw new IllegalArgumentException("No recommenders available.");
		}

		/*
		 * Get recommendation from first recommender.
		 */
		final SortedSet<R> firstRecommendations = firstRecommender.getRecommendation(entity);
		log.debug("got " + firstRecommendations.size() + " recommendations from " + firstRecommender);
		if (log.isDebugEnabled()) {
			log.debug(firstRecommendations);
		}

		/*
		 * Get recommendation from second recommender.
		 * 
		 * Since we need to get the scores from this recommender for the results from the first
		 * recommender, we use the MapBackedSet, such that we can easily get results by their name.
		 */
		final MapBackedSet<String, R> secondRecommendations = new MapBackedSet<String, R>(new TopResultsMapBackedSet.DefaultKeyExtractor<R>());
		secondRecommender.addRecommendation(secondRecommendations, entity);
		log.debug("got " + secondRecommendations.size() + " recommendations from " + secondRecommender);

		final double minScore = doFirstRound(recommendationResults, firstRecommendations, secondRecommendations); 
		log.debug("used " + recommendationResults.size() + " results from the first recommender which occured in second recommender");

		final int ctr = doSecondRound(recommendationResults, firstRecommendations, secondRecommendations, minScore); 
		log.debug("used another " + ctr + " results from the first recommender ");

		final SortedSet<R> thirdRecommendations = thirdRecommender.getRecommendation(entity);
		log.debug("got " + thirdRecommendations.size() + " recommendations from " + thirdRecommender);
		doThirdRound(recommendationResults, thirdRecommendations, minScore, ctr);

		if (log.isDebugEnabled()) {
			log.debug("final recommendation: " + recommendationResults);
		}
	}

	@Override
	public String getInfo() {
		return "Using the results from the second recommender to weight the results from the first recommender; if necessary, fill up with recommendations from third recommender.";
	}
	
	/**
	 * @return The third recommender.
	 */
	public Recommender<E, R> getThirdRecommender() {
		return this.thirdRecommender;
	}

	/**
	 * This recommender is used to fill up the recommendations, if the first 
	 * recommender can't provide enough results.
	 * @param thirdRecommender
	 */
	public void setThirdRecommender(Recommender<E, R> thirdRecommender) {
		this.thirdRecommender = thirdRecommender;
	}
}
