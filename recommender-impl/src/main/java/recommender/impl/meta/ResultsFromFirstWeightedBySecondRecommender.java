/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;

/**
 * Takes the results from {@link #firstRecommender} and orders them by their 
 * scores from {@link #secondRecommender}. If they're not recommended by 
 * {@link #secondRecommender}, they get a lower score. If 
 * {@link #firstRecommender} can't deliver enough results, they're filled up 
 * with the top results from {@link #secondRecommender}.
 * 
 * @author rja
 * 
 * @param <E> the recommendation entity
 * @param <R> the recommendation
 */
public class ResultsFromFirstWeightedBySecondRecommender<E, R extends RecommendationResult> extends AbstractMetaRecommender<E, R> {
	private static final Log log = LogFactory.getLog(ResultsFromFirstWeightedBySecondRecommender.class);

	protected Recommender<E, R> firstRecommender;
	protected Recommender<E, R> secondRecommender;

	/**
	 * Initializes the recommender with the given recommenders.
	 * 
	 * @param firstRecommender
	 * @param secondRecommender
	 */
	public ResultsFromFirstWeightedBySecondRecommender(final Recommender<E, R> firstRecommender, final Recommender<E, R> secondRecommender) {
		super();
		this.firstRecommender = firstRecommender;
		this.secondRecommender = secondRecommender;
	}
	
	/**
	 * Don't initializes any recommenders - you have to call the setters!
	 */
	public ResultsFromFirstWeightedBySecondRecommender() {
		super();
	}
	
	@Override
	protected void addRecommendationInternal(final Collection<R> recommendations, final E entity) {
		if (firstRecommender == null || secondRecommender == null) {
			throw new IllegalArgumentException("No recommenders available.");
		}
		/*
		 * Get recommendation from first recommender.
		 */
		final SortedSet<R> firstRecommendations = firstRecommender.getRecommendation(entity);
		log.debug("got " + firstRecommendations.size() + " recommendations from " + firstRecommender);
		if (log.isDebugEnabled()) {
			log.debug(firstRecommendations);
		}

		/*
		 * Get recommendation from second recommender.
		 * 
		 * Since we need to get the scores from this recommender for the results from the first
		 * recommender, we use the TopResultsMapBackedSet, such that we can easily get results by their title.
		 * Additionally, we might need to fill up the results with the top results (according to 
		 * the RecommendationResultComparator) from the second recommender. We get those from the 
		 * TopResultsMapBackedSet, too. 
		 */
		final TopResultsMapBackedSet<R> secondRecommendationResults = new TopResultsMapBackedSet<R>(numberOfResultsToRecommend);
		secondRecommender.addRecommendation(secondRecommendationResults, entity);
		log.debug("got " + secondRecommendationResults.size() + " recommendations from " + secondRecommender);

		/*
		 * The scores from the results in the next 'fill up round' should be lower 
		 * as the scores from this 'round'. Thus, we find the smallest value 
		 */
		final double minScore = doFirstRound(recommendations, firstRecommendations, secondRecommendationResults); 
		log.debug("used " + recommendations.size() + " results from the first recommender which occured in second recommender");


		final int ctr = doSecondRound(recommendations, firstRecommendations, secondRecommendationResults, minScore); 
		log.debug("used another " + ctr + " results from the first recommender ");		

		
		/*
		 * If we have not enough recommendations, yet, add results from second until set is complete.
		 */
		if (recommendations.size() < numberOfResultsToRecommend) {
			/*
			 * we want to get the top recommendations, not ordered alphabetically!
			 */
			final SortedSet<R> topRecommendationResults = secondRecommendationResults.getTopResults();
			doThirdRound(recommendations, topRecommendationResults, minScore, recommendations.size());
		}
		if (log.isDebugEnabled()) {
			log.debug("final recommendation: " + recommendations);
		}
	}

	protected double doFirstRound(final Collection<R> recommendationResults, final SortedSet<R> firstRecommendationResults, final MapBackedSet<String, R> secondRecommendationResults) {
		/* 
		 * First round:
		 * Iterate over results from first recommender and check them against second recommender.
		 * Add only those results, which are contained in the second recommender
		 */
		final Iterator<R> iterator1 = firstRecommendationResults.iterator();
		/*
		 * We need to find the minimum to add the remaining recommendations with lower scores
		 */
		double minScore = Double.MAX_VALUE;
		while (recommendationResults.size() < numberOfResultsToRecommend && iterator1.hasNext()) {
			final R currentresult = iterator1.next();
			if (secondRecommendationResults.contains(currentresult)) {
				/*
				 * this result is also recommended by the second recommender: give it his score
				 */

				final R secondRecommendationResult = secondRecommendationResults.get(currentresult);
				recommendationResults.add(secondRecommendationResult);
				/*
				 * remember minimal score
				 */
				final double score = secondRecommendationResult.getScore();
				if (score < minScore) minScore = score;
				/*
				 * remove result, such that don't use it again in the second round
				 */
				iterator1.remove();
			}
		}
		/*
		 * We would like to have values not larger than 1.0 ... but basically
		 * this prevents to have minScore = Double.MAX_VALUE, when no results from
		 * the first recommender were recommended by the second recommender.
		 */
		if (minScore > 1.0) minScore = 1.0; 
		return minScore;
	}

	protected int doSecondRound(final Collection<R> recommendationResults, final SortedSet<R> firstRecommendationResults, final MapBackedSet<String, R> secondRecommendationResults, final double minScore) {
		/*
		 * Second round:
		 * add remaining results from first recommender, scored lower than the results before
		 */
		final Iterator<R> iterator2 = firstRecommendationResults.iterator();
		int ctr = 0;
		while (recommendationResults.size() < numberOfResultsToRecommend && iterator2.hasNext()) {
			final R result = iterator2.next();
			ctr++;
			result.setScore(getLowerScore(minScore, ctr));
			recommendationResults.add(result);
		}
		return ctr;
	}

	protected int doThirdRound(final Collection<R> recommendationResults, final SortedSet<R> thirdRecommendationResults, final double minScore, final int ctr) {
		/*
		 * Third round:
		 * If we have not enough results, yet, add results from third recommender until set is complete.
		 */
		int myCtr = ctr;
		final Iterator<R> iterator3 = thirdRecommendationResults.iterator();
		while (recommendationResults.size() < numberOfResultsToRecommend && iterator3.hasNext()) {
			final R results = iterator3.next();
			if (!recommendationResults.contains(results)) {
				/*
				 * result has not already been added -> set its score lower than min
				 */
				myCtr++;
				results.setScore(getLowerScore(minScore, myCtr));
				recommendationResults.add(results);
			}
		}
		return myCtr;

	}
	

	/**
	 * Goal of this method: "append" not so good results on already recommended ("good") results 
	 * by ensuring that their score is lower than the "good" results.
	 * 
	 * Depending on the sign of the min score of the already recommended results, we apply
	 * a strategy to use the ctr as score. 
	 * 
	 * @param minScore
	 * @param ctr
	 * @return
	 */
	private static double getLowerScore(double minScore, int ctr) {
		final double newScore;
		if (minScore > 0) {
			/*
			 * go closer to zero (and don't do 'min/1 = min', thus '/ctr + 1')
			 */
			newScore = minScore / (ctr + 1);
		} else {
			/*
			 * go closer to -infinity
			 */
			newScore = minScore - ctr;
		}
		return newScore;
	}
	
	@Override
	public String getInfo() {
		return "Using the results from the second recommender to weight the recommended results from the first recommender.";
	}

	/**
	 * @return The first recommender.
	 */
	public Recommender<E, R> getFirstRecommender() {
		return this.firstRecommender;
	}


	/** This recommender's results are ordered by their respective score
	 * from the second recommender. 
	 * 
	 * @param firstRecommender
	 */
	public void setFirstRecommender(Recommender<E, R> firstRecommender) {
		this.firstRecommender = firstRecommender;
	}


	/**
	 * @return The second recommender.
	 */
	public Recommender<E, R> getSecondRecommender() {
		return this.secondRecommender;
	}


	/**
	 * The scores of this recommender are used to weight the recommendations from the first
	 * recommender.
	 *  
	 * @param secondRecommender
	 */
	public void setSecondRecommender(Recommender<E, R> secondRecommender) {
		this.secondRecommender = secondRecommender;
	}

	@Override
	protected void setFeedbackInternal(E entity, R result) {
		this.firstRecommender.setFeedback(entity, result);
		this.secondRecommender.setFeedback(entity, result);
	}
}
