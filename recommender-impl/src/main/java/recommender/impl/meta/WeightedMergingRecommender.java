/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;

/**
 * Merges and weights the recommendations of the given recommenders. 
 * 
 *   <p>
 *   The {@link #recommenders} array shall include all the recommenders which should 
 *   be queried. Each recommender's result scores and confidences are weighted by the corresponding 
 *   value in {@link #weights}.
 *   </p>
 *   
 *   <p>The weights should sum up to 1. If no weights are given, the score (confidence) of each result 
 *   is multiplied by 1 and added.
 *   
 * 
 * @author rja
 * 
 * @param <E> the recommendation entity
 * @param <R> the recommendation
 */
public class WeightedMergingRecommender<E, R extends RecommendationResult> extends AbstractMetaRecommender<E, R> {

	private List<Recommender<E, R>> recommenders;
	private double[] weights;

	/**
	 * Initializes the recommenders with a new {@link ArrayList}
	 */
	public WeightedMergingRecommender() {
		this.recommenders = new ArrayList<Recommender<E,R>>();
	}

	@Override
	protected void addRecommendationInternal(final Collection<R> recommendationResults, final E entity) {
		if (this.recommenders == null) {
			throw new IllegalArgumentException("No recommenders available.");
		}

		final TopResultsMapBackedSet<R> result = new TopResultsMapBackedSet<R>(this.numberOfResultsToRecommend);
		/*
		 * iterate over all given recommenders
		 */
		for (int i = 0; i < recommenders.size(); i++) {
			final Recommender<E, R> recommender = recommenders.get(i);
			final double scoreWeight = (weights != null || weights.length == recommenders.size()) ? weights[i] : 1;

			final SortedSet<R> tempRecommendationResults = recommender.getRecommendation(entity);
			/*
			 * iterate over all recomendationss and add them to result
			 */
			for (final R recommendationResult : tempRecommendationResults) {
				addResult(result, recommendationResult, scoreWeight);
			}
		}

		/*
		 * copy result map into sorted set
		 */
		recommendationResults.addAll(result.getTopResults());
	}

	/**
	 * Adds a recommendation to the result.
	 * 
	 * If the recommendation is not already contained: multiplies the score and confidence 
	 * of the recommendation with the weigth and adds the recommendation to the result.
	 * 
	 * Otherwise, multiplies the score and confidence with the weight and adds 
	 * the result to the score/confidence of the existing recommendation.
	 * 
	 * @param result - the map into which the recommendationResult should be put 
	 * @param recommendationResult
	 * @param weight the weight the score/confidence of the recommendation should be weighted with.
	 */
	private void addResult(final TopResultsMapBackedSet<R> result, final R recommendationResult, final double weight) {
		final double score = recommendationResult.getScore() * weight;
		final double confidence = recommendationResult.getConfidence() * weight;

		if (result.contains(recommendationResult)) {
			/*
			 * add score and confidence
			 */
			final R recommendationResult2 = result.get(recommendationResult);
			recommendationResult2.setScore(recommendationResult2.getScore() + score);
			recommendationResult2.setConfidence(recommendationResult2.getConfidence() + confidence);
		} else {
			/*
			 * create new recommendationResult with weighted score and confidence
			 */
			recommendationResult.setScore(score);
			recommendationResult.setConfidence(confidence);
			result.add(recommendationResult);
		}
	}
	
	@Override
	public String getInfo() {
		return "Merges and weights the recommendations of the given recommenders.";
	}

	/**
	 * @return The weights used to weight the score/confidence of each recommendation of each recommender.
	 */
	public double[] getWeights() {
		return this.weights;
	}

	/** The score/confidence of each recommendation from the recommenders in {@link #recommenders} is
	 * weighted by the corresponding weight in {@link #weights}. 
	 *  
	 * @param weights
	 */
	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	/**
	 * @return The recommenders used by this recommender.
	 */
	public List<Recommender<E, R>> getRecommenders() {
		return this.recommenders;
	}

	/**
	 * Give this recommender an array of recommenders it will query.
	 * 
	 * @param recommenders
	 */
	public void setRecommenders(List<Recommender<E, R>> recommenders) {
		this.recommenders = recommenders;
	}

	@Override
	protected void setFeedbackInternal(E entity, R result) {
		for (final Recommender<E, R> reco : recommenders) {
			reco.setFeedback(entity, result);
		}
	}
}
