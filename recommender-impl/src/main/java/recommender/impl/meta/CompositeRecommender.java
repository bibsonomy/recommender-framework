/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;

/**
 * Recommender which queries different recommenders one after another and integrates their results into one list.
 * 
 * @author rja
 * 
 * @param <E> the recommendation entity
 * @param <R> the recommendation result
 */
public class CompositeRecommender<E, R extends RecommendationResult> implements Recommender<E, R> {

	private final List<Recommender<E, R>> recommenders = new LinkedList<Recommender<E, R>>();
	private final Comparator<R> comparator;
	
	private int numberOfResultsToRecommend = Recommender.DEFAULT_NUMBER_OF_RESULTS_TO_RECOMMEND;
	
	/**
	 * Create a new instance of this class. The comparator is necessary to fill the
	 * SortedSet in {@link #getRecommendation(E)}.
	 * 
	 * @param comparator
	 */
	public CompositeRecommender(final Comparator<R> comparator) {
		this.comparator = comparator;
	}
	
	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#getRecommendation(recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public SortedSet<R> getRecommendation(E entity) {
		final SortedSet<R> recommendationResults = new TreeSet<R>(comparator);
		addRecommendation(recommendationResults, entity);
		return recommendationResults;
	}

	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#getInfo()
	 */
	@Override
	public String getInfo() {
		// TODO: add all infos of the recommenders to the info output?
		return "Generic composite scraper.";
	}

	/**
	 * Adds a recommender to the list of recommenders.
	 * 
	 * @param recommender
	 */
	public void addRecommender(final Recommender<E, R> recommender) {
		recommender.setNumberOfResultsToRecommend(this.numberOfResultsToRecommend);
		this.recommenders.add(recommender);
	}

	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#addRecommendation(java.util.Collection, recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public void addRecommendation(Collection<R> recommendationResults, E entity) {
		for (final Recommender<E, R> recommender : this.recommenders) {
			recommender.addRecommendation(recommendationResults, entity);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#setFeedback(recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public void setFeedback(final E entity, final R result) {
		for (final Recommender<E, R> recommender : this.recommenders) {
			recommender.setFeedback(entity, result);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#setNumberOfResultsToRecommend(int)
	 */
	@Override
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend) {
		this.numberOfResultsToRecommend = numberOfResultsToRecommend;
		// TODO: reset also all recommenders?
	}
}
