/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.meta;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.util.RecommendationResultComparator;

/**
 * The basic skeleton to implement a meta recommender.
 * 
 * @author rja
 * @param <E> the recommendation entity
 * @param <R> the recommendation result
 */
public abstract class AbstractMetaRecommender<E, R extends RecommendationResult> implements Recommender<E, R> {
	private static final Log log = LogFactory.getLog(AbstractMetaRecommender.class);
	
	/**
	 * The maximal number of results the recommender shall return on a call to
	 * {@link #getRecommendation(Entity)}.
	 */
	protected int numberOfResultsToRecommend = Recommender.DEFAULT_NUMBER_OF_RESULTS_TO_RECOMMEND;

	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#getRecommendation(recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public SortedSet<R> getRecommendation(final E entity) {
		final SortedSet<R> recommendaationResults = new TreeSet<R>(new RecommendationResultComparator<R>());
		this.addRecommendation(recommendaationResults, entity);
		
		return recommendaationResults;
	}

	/**
	 * @return The (maximal) number of results this recommender shall return.
	 */
	public int getNumberOfResultsToRecommend() {
		return this.numberOfResultsToRecommend;
	}
	
	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#addRecommendation(java.util.Collection, recommender.core.interfaces.model.RecommendationEntity)
	 */
	@Override
	public void addRecommendation(final Collection<R> recommendationResults, final E entity) {
		log.debug("Getting meta recommendations for " + entity);
		this.addRecommendationInternal(recommendationResults, entity);
		if (log.isDebugEnabled()) log.debug("Recommending results " + recommendationResults);
	}
	
	protected abstract void addRecommendationInternal(Collection<R> recommendationResults, E entity);

	@Override
	public void setFeedback(E entity, R result) {
		log.debug("got entity with id " + entity + " as feedback.");
		this.setFeedbackInternal(entity, result);
	}
	
	protected abstract void setFeedbackInternal(E entity, R result);
	
	/*
	 * (non-Javadoc)
	 * @see recommender.core.Recommender#setNumberOfResultsToRecommend(int)
	 */
	@Override
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend) {
		this.numberOfResultsToRecommend = numberOfResultsToRecommend;
	}
}