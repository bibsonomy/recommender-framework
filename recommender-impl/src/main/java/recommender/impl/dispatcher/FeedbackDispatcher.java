/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.dispatcher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.impl.multiplexer.MultiplexingRecommender;

/**
 * Class for dispatching recommendation feedback queries.
 * 
 * @param <E>
 * @param <R>
 */
public class FeedbackDispatcher<E, R extends RecommendationResult> extends Thread {
	private static final Log log = LogFactory.getLog(FeedbackDispatcher.class);
	
	private final Recommender<E, R> recommender;
	private boolean abort = false;
	private E entity;
	private R result;

	/**
	 * Constructor for creating a query dispatcher.
	 * @param recommender Recommender whos query should be dispatched
	 * @param entity entity to query the recommender for
	 * @param result 
	 */
	public FeedbackDispatcher(final Recommender<E, R> recommender, final E entity, final R result) {
		this.recommender = recommender;
		this.entity = entity;
		this.result = result;
		MultiplexingRecommender.incFeedbackCounter();
	}

	/**
	 * Get managed recommender's info.
	 * @return recommender's info text
	 */
	public String getInfo() {
		// just return recommenders info
		return this.recommender.getInfo();
	}

	/**
	 * Dispatch and collect query.
	 */
	@Override
	public void run() {
		// for query-time logging
		long time = System.currentTimeMillis();
		// actually query the recommender
		try {
			this.recommender.setFeedback(this.entity, this.result);
		} catch( final Exception e ) {
			log.error("Error setting feedback for recommender " + this.recommender.getInfo(), e);
		}
		time = System.currentTimeMillis()-time;
		if( !this.abort ) {
			log.info("run finished in time " + time);
		} else {
			log.info("Setting feedback for recommender " + this.recommender.getInfo() + " timed out (" + time + ")");
		}
		MultiplexingRecommender.decFeedbackCounter();
	}

	/**
	 * Tell dispatcher that he timed out.
	 */
	public void abortQuery() {
		this.abort = true;
	}
	
	public void setResult(R result) {
		this.result = result;
	}
	
	public void setEntity(E entity) {
		this.entity = entity;
	}
}
