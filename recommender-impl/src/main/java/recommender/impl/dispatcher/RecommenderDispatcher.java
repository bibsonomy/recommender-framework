/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.impl.dispatcher;

import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.util.RecommendationResultComparator;
import recommender.impl.multiplexer.MultiplexingRecommender;

/**
 * Class for dispatching recommendation queries.
 * 
 * @param <E>
 * @param <R>
 */
public class RecommenderDispatcher<E, R extends RecommendationResult> extends Thread {
	private static final Log log = LogFactory.getLog(RecommenderDispatcher.class);
	
	/** unique id identifying set of queries */
	private final Long qid;
	/** unique id identifying recommender */
	private final Long sid;
	private final Recommender<E, R> recommender;
	private boolean abort = false;
	private E entity;
	private SortedSet<R> recommendationResults;
	private MultiplexingRecommender<E, R> multiplexer;

	/**
	 * Constructor for creating a query dispatcher.
	 * @param recommender Recommender who's query should be dispatched
	 * @param entity entity to query the recommender for
	 * @param qid unique id identifying set of queries
	 * @param sid 
	 * @param recommendationResults previously recommended results
	 * @param multiplexer 
	 */
	public RecommenderDispatcher(final Recommender<E, R> recommender, final E entity, final Long qid, final Long sid, final SortedSet<R> recommendationResults, final MultiplexingRecommender<E, R> multiplexer) {
		this.recommender = recommender;
		this.entity = entity;
		this.qid = qid;
		this.sid = sid;
		this.recommendationResults = recommendationResults;
		this.multiplexer = multiplexer;
		
		MultiplexingRecommender.incQueryCounter();
	}

	/**
	 * Get managed recommender's info.
	 * @return recommender's info text
	 */
	public String getInfo() {
		// just return recommenders info
		return this.recommender.getInfo();
	}

	/**
	 * Dispatch and collect query.
	 */
	@Override
	public void run() {
		// for query-time logging
		long time = System.currentTimeMillis();
		// actually query the recommender
		try {
			if ((this.recommendationResults != null) && (this.recommendationResults.size() > 0)) {
				final SortedSet<R> preset = new TreeSet<R>(new RecommendationResultComparator<R>());
				preset.addAll(this.recommendationResults);
				this.recommender.addRecommendation(this.recommendationResults, this.entity);
			} else {
				this.recommendationResults = this.recommender.getRecommendation(this.entity);
			}
		} catch (final Exception e) {
			log.error("(" + this.qid + ") Error querying recommender " + this.recommender.getInfo(), e);
		}
		// calculate query-time
		time = System.currentTimeMillis()-time;
		
		// add query result
		this.multiplexer.addQueryResponse(this.qid, this.sid, time, this.recommendationResults);
		
		if (!this.abort) {
			log.info("(" + this.qid + ") run finished in time " + time);
		} else {
			log.info("(" + this.qid + ") Recommender " + this.recommender.getInfo() + " timed out (" + time + ")");
		}
		
		MultiplexingRecommender.decQueryCounter();
	}
	/**
	 * Tell dispatcher that he timed out.
	 */
	public void abortQuery() {
		this.abort = true;
	}
}
