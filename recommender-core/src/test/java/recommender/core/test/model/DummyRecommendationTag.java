/**
 * Recommender Implementation - Multiplexer and web service recommender interface
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.test.model;

import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.model.RecommendationTag;

/**
 * Dummy implementation of a {@link RecommendationTag} for testing purpose.
 * 
 * @author lukas
 *
 */
public class DummyRecommendationTag implements RecommendationTag, RecommendationResult {

	private String name;
	private double score;
	private double confidence;

	/**
	 * default constructor
	 */
	public DummyRecommendationTag() {
		// noop
	}

	public DummyRecommendationTag(final String name, final double score, final double confidence) {
		this.setName(name);
		this.setConfidence(confidence);
		this.setScore(score);
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(RecommendationTag o) {
		return this.name.compareToIgnoreCase(o.getName());
	}

	@Override
	public double getScore() {
		return this.score;
	}

	@Override
	public double getConfidence() {
		return this.confidence;
	}

	@Override
	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}

	@Override
	public String getRecommendationId() {
		return this.name;
	}

	@Override
	public boolean equals(Object tag) {
		/*
		 * if tag is null or not a RecommendedTag, return false
		 */
		if (!(tag instanceof DummyRecommendationTag)) {
			return false;
		}

		/*
		 * cast
		 */
		final DummyRecommendationTag recTag = (DummyRecommendationTag) tag;

		/*
		 * accept the super classes 'equals' method
		 */
		if (super.equals(recTag)) return true;

		/*
		 * ignore case
		 */
		return this.getName().equalsIgnoreCase((recTag).getName());
	}

	@Override
	public int compareToOtherRecommendationResult(final RecommendationResult o) {
		if (o instanceof DummyRecommendationTag) {
			final DummyRecommendationTag otherRecommendedTag = (DummyRecommendationTag) o;
			return this.getName().compareTo(otherRecommendedTag.getName());
		}
		return -1;
	}

	@Override
	public String getTitle() {
		return this.getName();
	}
}
