package recommender.core.util;

import org.junit.Test;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.model.RecommendationTag;
import recommender.core.test.model.DummyRecommendationTag;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.*;

/**
 * @author rja
 */
public class RecommendationResultComparatorTest {

	private final static DummyRecommendationTag[] tags = new DummyRecommendationTag[] {
			new DummyRecommendationTag("d", 1.0, 0.0),
			new DummyRecommendationTag("c", 0.5, 0.0),
			new DummyRecommendationTag("b", 0.25, 0.0),
			new DummyRecommendationTag("a", 0.125, 0.0)
	};

	/**
	 * Tests {@link DummyRecommendationTag#equals(Object)}.
	 */
	@Test
	public void testEquals() {
		assertFalse(tags[0].equals(tags[1]));
	}

	/**
	 * Tests {@link DummyRecommendationTag#compareTo(RecommendationTag)}
	 */
	@Test
	public void testCompare1() {
		/*
		 * a < b < c < d
		 * (including transitivity)
		 */
		assertTrue(tags[0].compareTo(tags[1]) > 0);
		assertTrue(tags[0].compareTo(tags[2]) > 0);
		assertTrue(tags[0].compareTo(tags[3]) > 0);
		assertTrue(tags[1].compareTo(tags[2]) > 0);
		assertTrue(tags[1].compareTo(tags[3]) > 0);
		assertTrue(tags[2].compareTo(tags[3]) > 0);
	}

	/**
	 * Tests {@link RecommendationResultComparator#compare(RecommendationResult, RecommendationResult)}.
	 */
	@Test
	public void testCompare2() {
		final RecommendationResultComparator comp = new RecommendationResultComparator();
		/*
		 * We want the tag with the highest score to be first!
		 *
		 * 1.0 < 0.5 < 0.25 < 0.125
		 * (including transitivity)
		 *
		 */
		assertTrue(comp.compare(tags[0], tags[1]) < 0);
		assertTrue(comp.compare(tags[0], tags[2]) < 0);
		assertTrue(comp.compare(tags[0], tags[3]) < 0);
		assertTrue(comp.compare(tags[1], tags[2]) < 0);
		assertTrue(comp.compare(tags[1], tags[3]) < 0);
		assertTrue(comp.compare(tags[2], tags[3]) < 0);
	}


	@Test
	public void testCompare3() {
		final SortedSet<DummyRecommendationTag> testSet = new TreeSet<DummyRecommendationTag>(new RecommendationResultComparator<DummyRecommendationTag>());

		for (final DummyRecommendationTag t: tags) {
			testSet.add(t);
			/*
			 * tag with highest score should always be first
			 */
			assertEquals(tags[0], testSet.first());
		}
	}

	/**
	 * Test that tags with t1.equalsIgnoreCase(t2) get lost.
	 *
	 * Changes:
	 * - 2009-04-14: case of tags is now ignored!
	 */
	@Test
	public void testCompare4() {
		final SortedSet<DummyRecommendationTag> testSet = new TreeSet<DummyRecommendationTag>(new RecommendationResultComparator<DummyRecommendationTag>());

		testSet.add(new DummyRecommendationTag("main", 0.0, 0.0));
		testSet.add(new DummyRecommendationTag("Main", 0.0, 0.0));

		assertEquals(1, testSet.size());

		assertTrue(testSet.contains(new DummyRecommendationTag("main", 0.0, 0.0)));
		assertTrue(testSet.contains(new DummyRecommendationTag("Main", 0.0, 0.0)));
	}


	@Test
	public void testOrder() {
		final SortedSet<DummyRecommendationTag> tags = new TreeSet<DummyRecommendationTag>(new RecommendationResultComparator<DummyRecommendationTag>());
		tags.add(new DummyRecommendationTag("eins", 0.3, 0.2));
		tags.add(new DummyRecommendationTag("drei", 0.2, 0.2));
		tags.add(new DummyRecommendationTag("vier", 0.5, 0.2));
		tags.add(new DummyRecommendationTag("sieben", 0.6, 0.2));
		tags.add(new DummyRecommendationTag("eins", 0.5, 0.2));
		tags.add(new DummyRecommendationTag("eins", 0.2, 0.2));
		tags.add(new DummyRecommendationTag("semantic", 0.5, 0.2));
		tags.add(new DummyRecommendationTag("bar", 0.6, 0.2));
		tags.add(new DummyRecommendationTag("foo", 0.7, 0.2));
		tags.add(new DummyRecommendationTag("net", 0.8, 0.2));

		final Iterator<DummyRecommendationTag> iterator = tags.iterator();

		assertEquals("net", iterator.next().getName());
		assertEquals("foo", iterator.next().getName());
		assertEquals("bar", iterator.next().getName());
		assertEquals("sieben", iterator.next().getName());
		assertEquals("semantic", iterator.next().getName());
		assertEquals("vier", iterator.next().getName());
		assertEquals("eins", iterator.next().getName());
		assertEquals("drei", iterator.next().getName());
		assertFalse(iterator.hasNext());
	}

}