
-- Erstellungszeit: 08. Aug 2013 um 14:43

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `recommender`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `log_recommender`
--

CREATE TABLE IF NOT EXISTS `log_recommender` (
  `query_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_id` varchar(255) NOT NULL DEFAULT '-1',
  `user_name` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content_type` varchar(255) NOT NULL,
  `timeout` int(5) DEFAULT '1000',
  PRIMARY KEY (`query_id`),
  KEY `entity_id_user_name_date` (`entity_id`,`user_name`,`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=714 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_feedback`
--

CREATE TABLE IF NOT EXISTS `recommender_feedback` (
  `entity_id` varchar(255) NOT NULL,
  `result_id` varchar(255) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`entity_id`,`user_name`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_log_entities`
--

CREATE TABLE IF NOT EXISTS `recommender_log_entities` (
  `RequestId` int(10) NOT NULL,
  `EntityId` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `log_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=119 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_preset`
--

CREATE TABLE IF NOT EXISTS `recommender_preset` (
  `query_id` varchar(20) NOT NULL,
  `setting_id` bigint(20) NOT NULL,
  `resultId` varchar(255) NOT NULL,
  PRIMARY KEY (`query_id`,`setting_id`,`resultId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_querymap`
--

CREATE TABLE IF NOT EXISTS `recommender_querymap` (
  `query_id` bigint(20) NOT NULL,
  `setting_id` bigint(20) NOT NULL,
  PRIMARY KEY (`query_id`,`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_recommendations`
--

CREATE TABLE IF NOT EXISTS `recommender_recommendations` (
  `query_id` bigint(20) NOT NULL,
  `score` double NOT NULL,
  `confidence` double NOT NULL,
  `responsetitle` varchar(255) NOT NULL,
  `responseid` varchar(255) NOT NULL,
  PRIMARY KEY (`query_id`,`responsetitle`,`responseid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_result`
--

CREATE TABLE IF NOT EXISTS `recommender_result` (
  `result_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `query_id` bigint(20) NOT NULL,
  `setting_id` bigint(20) NOT NULL,
  `rec_latency` int(11) DEFAULT NULL,
  `score` double NOT NULL,
  `confidence` double NOT NULL,
  `responsetitle` varchar(255) NOT NULL,
  `responseid` varchar(255) NOT NULL,
  PRIMARY KEY (`result_id`),
  UNIQUE KEY `result_id` (`result_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=382 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_selection`
--

CREATE TABLE IF NOT EXISTS `recommender_selection` (
  `query_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `setting_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`query_id`,`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_selectormap`
--

CREATE TABLE IF NOT EXISTS `recommender_selectormap` (
  `query_id` bigint(20) NOT NULL,
  `selector_id` bigint(20) NOT NULL,
  PRIMARY KEY (`query_id`,`selector_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_selectors`
--

CREATE TABLE IF NOT EXISTS `recommender_selectors` (
  `selector_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `selector_name` varchar(50) NOT NULL,
  `selector_meta` blob,
  PRIMARY KEY (`selector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_settings`
--

CREATE TABLE IF NOT EXISTS `recommender_settings` (
  `setting_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rec_id` varchar(255) NOT NULL,
  `rec_meta` blob,
  `rec_descr` varchar(255) DEFAULT NULL,
  `trusted` int(1) NOT NULL default '0',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_settings_log`
--

CREATE TABLE IF NOT EXISTS `recommender_settings_log` (
  `setting_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rec_id` varchar(255) NOT NULL,
  `rec_meta` blob,
  `rec_descr` varchar(255) DEFAULT NULL,
  `trusted` int(1) NOT NULL default '0',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `recommender_status`
--

CREATE TABLE IF NOT EXISTS `recommender_status` (
  `setting_id` bigint(20) unsigned NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `local` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
