/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.util;

import java.util.Comparator;

import recommender.core.interfaces.model.RecommendationResult;


/** Compares two recommendation results.
 * Results are ordered by their score (confidence not considered).
 * <br/>Two results are equal based on {@link RecommendationResult#equals(Object)} - 
 * independent of their scores!   
 * 
 * @author rja
 * @param <R> 
 */
public class RecommendationResultComparator<R extends RecommendationResult> implements Comparator<R> {

	@Override
	public int compare(final R o1, final R o2) {
		if (o1 == null) return -1;
		if (o2 == null) return 1;
		/*
		 * result titles equal: regard them as equal
		 * (this basically ensures that a set won't contain results which equal based
		 *  on their equals() method)
		 */
		if (o1.equals(o2)) return 0;
		/*
		 * the highest score should come first (in the set) - hence, 
		 * do o2 - o1 
		 */
		int signum = new Double(Math.signum(o2.getScore() - o1.getScore())).intValue();
		if (signum != 0) return signum;
		/*
		 * scores equal: consider confidence
		 */
		signum = new Double(Math.signum(o2.getConfidence() - o1.getConfidence())).intValue();
		if (signum != 0) return signum;
		/*
		 * scores and confidence equal (but titles not): return using compareTo from Tag.
		 */
		return o1.compareToOtherRecommendationResult(o2);
	}
}
