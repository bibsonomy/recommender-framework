/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.interfaces;

import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;


/**
 * This interface should provide access to different remote functions. It is mandatory for querying remote recommenders.
 * 
 * @author fei
 * 
 * @param <E> the recommendation entity
 * @param <R> the recommendation
 */
public interface RecommenderConnector<E, R extends RecommendationResult> extends Recommender<E, R> {
	/** post parameter for the feedback model */
	public static final String ID_FEEDBACK = "feedback";
	/** post parameter for the recommendation  model */
	public static final String ID_RECQUERY = "data";
	
	/** url map for the getRecommendation method */
	public static final String METHOD_GETRECOMMENDEDTAGS = "recommendations";
	
	/** url map for the setFeedback method */
	public static final String METHOD_SETFEEDBACK = "feedback";

	/**
	 * Description for identifying this recommender.
	 * @return the id
	 */
	public String getId();
	
	/**
	 * @return the recommendation renderer of this remote connector
	 */
	public RecommendationRenderer<E, R> getRecommendationRenderer();
	
	/**
	 * @param renderer the recommendation renderer to use
	 */
	public void setRecommendationRenderer(RecommendationRenderer<E, R> renderer);

	/**
	 * @return <code>true</code> iff the recommendation framework can trust the recommender
	 * (if <code>false</code> recommender gets called only with anonymized data)
	 */
	public boolean isTrusted();
}
