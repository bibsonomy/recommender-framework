/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.interfaces.renderer;

import java.io.Reader;
import java.io.Writer;
import java.util.List;
import java.util.SortedSet;

import recommender.core.error.BadRequestOrResponseException;
import recommender.core.interfaces.model.RecommendationResult;

/**
 * This interface is mandatory for remote recommender access.
 * It should provide the serialization and deserialization of
 * {@link RecommendationResult}s
 * 
 * @author Manuel Bork <manuel.bork@uni-kassel.de>
 * 
 * @param <E> the recommendation entity
 * @param <R> the recommendation
 */
public interface RecommendationRenderer<E, R extends RecommendationResult> {

	/**
	 * Serializes one {@link #E}.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param entity
	 *            one {@link RecommendationEntity} object.
	 * @param model
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeRecommendationEntity(Writer writer, E entity);
	
	/**
	 * @param reader
	 * @return the entity result
	 * @throws BadRequestOrResponseException 
	 */
	public E parseRecommendationEntity(final Reader reader) throws BadRequestOrResponseException;
	
	/**
	 * @param writer a writer to use
	 * @param recommendations the recommendations to write
	 */
	public void serializeRecommendationResultList(Writer writer, SortedSet<R> recommendations);
	
	/**
	 * Reads a List of {@link RecommendationResult}s from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a {@link List} of {@link RecommendationResult} objects.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public SortedSet<R> parseRecommendationResultList(Reader reader) throws BadRequestOrResponseException;
	
	/**
	 * Reads the status from a {@link Reader}
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return the status ("ok" or "fail")
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parseStat(Reader reader) throws BadRequestOrResponseException;

	/**
	 * @return the content type the renderer serializes and parses
	 */
	public String getContentType();

	/**
	 * @param writer
	 */
	public void serializeOK(Writer writer);

}