/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.interfaces.filter;


/**
 * This filter allows to implement a privacy check, to prevent
 * forwarding of private data to external services
 * 
 * This interface is mandatory to implement
 * 
 * If you've got sensible data it should be implemented very sophisticated
 * @param <E> the {@link RecommendationEntity} to filter
 */
public interface PrivacyFilter<E> {

	/**
	 * Filter the entity and return null if it should not be forwarded (private)
	 * Otherwise this method should return a copy of the entity.
	 * 
	 * @param entity the entity to filer
	 * @return a copy of the entity if it's not private, null otherwise
	 */
	public E filterEntity(E entity);
}
