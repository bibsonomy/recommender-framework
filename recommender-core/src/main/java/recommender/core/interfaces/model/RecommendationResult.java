/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.interfaces.model;

/**
 * Interface which provides access to results of recommendations. All results which shall be produces by the <br>
 * recommender must implement this interface.
 * 
 * @author lukas
 *
 */
public interface RecommendationResult {

	/**
	 * @return the score of the result
	 */
	public double getScore();
	
	/**
	 * @return the confidence of the result
	 */
	public double getConfidence();
	
	/**
	 * @param score the score of the result
	 */
	public void setScore(double score);
	
	/**
	 * @param confidence the confidence of the result
	 */
	public void setConfidence(double confidence);
	
	/**
	 * @return the unique id of the result
	 */
	public String getRecommendationId();
	
	/**
	 * @see Comparable#compareTo(Object)
	 * 
	 * @param o
	 * @return 
	 */
	public int compareToOtherRecommendationResult(RecommendationResult o);
	
	/**
	 * @return the title of the result
	 */
	@Deprecated // TODO: why do we need this?
	public String getTitle();
}
