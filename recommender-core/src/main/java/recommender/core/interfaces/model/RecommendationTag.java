/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.interfaces.model;

/**
 * This interface is mandatory to implement.
 * It is the internal representation of the applications Tag model.
 * 
 * @author Lukas
 *
 */
public interface RecommendationTag extends Comparable<RecommendationTag> {
	
	/**
	 * should return the tag name linke e.g. "recommendation", "collaborative-filtering" or something
	 * like that
	 * 
	 * @return the tags name
	 */
	public String getName();
	
	/**
	 * sets the name of a tag (the name is what appears on the screen)
	 * 
	 * @param name the name to set
	 */
	public void setName(String name);
}
