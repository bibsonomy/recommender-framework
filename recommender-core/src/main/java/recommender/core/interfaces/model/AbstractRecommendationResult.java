/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.interfaces.model;

/**
 * abstract recommendation result
 *
 * @author dzo
 */
public abstract class AbstractRecommendationResult implements RecommendationResult {
	
	private double score;
	private double confidence;
	
	/**
	 * default constructor
	 */
	public AbstractRecommendationResult() {
		// noop
	}
	
	/**
	 * @param score
	 * @param confidence
	 */
	public AbstractRecommendationResult(double score, double confidence) {
		this.score = score;
		this.confidence = confidence;
	}

	/**
	 * @return the score
	 */
	@Override
	public double getScore() {
		return this.score;
	}
	
	/**
	 * @param score the score to set
	 */
	@Override
	public void setScore(double score) {
		this.score = score;
	}
	
	/**
	 * @return the confidence
	 */
	@Override
	public double getConfidence() {
		return this.confidence;
	}
	
	/**
	 * @param confidence the confidence to set
	 */
	@Override
	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}
}
