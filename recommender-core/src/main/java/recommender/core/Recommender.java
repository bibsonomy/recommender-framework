/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core;

import java.util.Collection;
import java.util.SortedSet;

import recommender.core.interfaces.model.RecommendationResult;

/**
 * the interface all recommenders should implement
 *
 * @author lukas
 * @param <E> the entity the recommendation should be based on (Post, User, …)
 * @param <R> the recommendation result (Tags, Posts, …)
 */
public interface Recommender<E, R extends RecommendationResult> {
	
	/** default value for the number of results to recommend */
	public static final int DEFAULT_NUMBER_OF_RESULTS_TO_RECOMMEND = 5;

	/**
	 * Provide recommendations for the given {@link E}.
	 * 
	 * @param entity A {@link E} describing the resource for which recommendations should be produced.
	 * The entity should contain an valid unique id as well as the name of the querying 
	 * user.
	 * 
	 * @return A list of {@link RecommendationResult} in descending order of their relevance.
	 */
	public SortedSet<R> getRecommendation(final E entity);
	
	/**
	 * Into the given collection, the recommender shall add its {@link RecommendationResult}.
	 * Then, {@link RecommendationResult} should contain the result of {@link #getRecommendation(E)}.
	 * <br/>
	 * The reason for having this method in addition to {@link #getRecommendation(E)} is
	 * to allow use of special collections and comparators.  
	 * 
	 * @see #getRecommendation(E)
	 * @param recommendations An empty collection in which to insert {@link RecommendationResult}.
	 * @param entity
	 */
	@Deprecated // use getRecommendation instead
	public void addRecommendation(final Collection<R> recommendations, final E entity);
	
	/**
	 * Finishes the recommendation process and provides feedback for the entity as 
	 * it will be stored.
	 * 
	 * @param entity The complete entity as it will be stored.
	 * @param result the result
	 */
	public void setFeedback(final E entity, final R result);
	
	/**
	 * specifies the number of recommendations results
	 * 
	 * @param numberOfResultsToRecommend the wanted count of recommendations
	 */
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend);
	
	/**
	 * Provide some short information about this recommender.
	 * 
	 * @return A short string describing the recommender.
	 */
	public String getInfo();
}
