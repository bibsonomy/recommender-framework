/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core;

import java.util.SortedSet;

import recommender.core.interfaces.model.RecommendationResult;

/**
 * TODO: add documentation to this class
 *
 * @author dzo
 * @param <E> 
 * @param <R> 
 */
public interface RecommendationService<E, R extends RecommendationResult> {
	
	/**
	 * @param userName
	 * @param entity
	 * @return the recommendations for the entity
	 */
	public SortedSet<R> getRecommendationsForUser(final String userName, final E entity);
	
	/**
	 * 
	 * @param userName
	 * @param entity
	 * @param result
	 */
	public void setFeedback(String userName, final E entity, final R result);

	/**
	 * @param numberOfResultsToRecommend
	 */
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend);
}
