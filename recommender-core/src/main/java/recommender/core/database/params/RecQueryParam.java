/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database.params;

import java.sql.Timestamp;

/**
 * Database parameter which represents a recommendation query.
 * @param <E> 
 */
public class RecQueryParam<E> {
	private Long qid;
	/** ID for mapping entities to recommender queries */
	private E entity;
	private String entity_id;
	/** content type of {@link RecommendationEntity} */
	private String contentType;
	private String userName;
	private Timestamp timeStamp;
	/** querie's timeout value */
	private int queryTimeout;
	
	/**
	 * @param timeStamp the query's timestamp
	 */
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	/**
	 * @return the query's timestamp
	 */
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	
	/**
	 * @param userName the username who triggered the query
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * @return the username who triggered the query
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * @param qid the query's id
	 */
	public void setQid(long qid) {
		this.qid = qid;
	}
	
	/**
	 * @return the query's id
	 */
	public long getQid() {
		return qid;
	}
	
	/**
	 * @param content_type the content type for which the recommendation was made
	 */
	public void setContentType(String content_type) {
		this.contentType = content_type;
	}
	
	/**
	 * @return the content type for which the recommendation was made
	 */
	public String getContentType() {
		return contentType;
	}
	
	/**
	 * @param entity the entity
	 */
	public void setEntity(E entity) {
		this.entity = entity;
	}
	
	/**
	 * @return the entity
	 */
	public E getEntity() {
		return entity;
	}
	
	/**
	 * @return the entity_id
	 */
	public String getEntity_id() {
		return this.entity_id;
	}

	/**
	 * @param entity_id the entity_id to set
	 */
	public void setEntity_id(String entity_id) {
		this.entity_id = entity_id;
	}

	/**
	 * @param queryTimeout the timeout which was specified for that query
	 */
	public void setQueryTimeout(int queryTimeout) {
		this.queryTimeout = queryTimeout;
	}
	
	/**
	 * @return the timeout which was specified for that query
	 */
	public int getQueryTimeout() {
		return queryTimeout;
	}
}
