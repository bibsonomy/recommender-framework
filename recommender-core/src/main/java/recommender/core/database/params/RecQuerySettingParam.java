/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database.params;

/**
 * Database parameter representing the mapping of a query to a setting id.
 * 
 * @author fei
 */
public class RecQuerySettingParam {
	/** query id */
	private Long qid;
	/** settings id */
	private Long sid;
	
	/**
	 * @param qid the query's id
	 */
	public void setQid(Long qid) {
		this.qid = qid;
	}
	
	/**
	 * @return the query's id
	 */
	public Long getQid() {
		return qid;
	}
	
	/**
	 * @param sid the setting's id
	 */
	public void setSid(Long sid) {
		this.sid = sid;
	}
	
	/**
	 * @return the setting's id
	 */
	public Long getSid() {
		return sid;
	}
}