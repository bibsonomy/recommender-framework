/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database.params;

/**
 * Database parameter for retrieving the latencies of recommenders
 * 
 * @author bsc
 */
public class LatencyParam {
	
    private Long sid;
    private Long numberOfQueries;
    
    /**
     * @param sid sid to set
     * @param numberOfQueries number of queries to set
     */
    public LatencyParam(Long sid, Long numberOfQueries){
    	this.setSettingID(sid);
    	this.setNumberOfQueries(numberOfQueries);
    }
    
    /**
     * @param sid seid to set
     */
    public void setSettingID(Long sid){
    	this.sid = sid;
    }
    
    /**
     * @return the sid
     */
    public Long getSettingID(){
    	return this.sid;
    }
    
    /**
     * @param numberOfQueries the number of queries to set
     */
    public void setNumberOfQueries(Long numberOfQueries){
    	this.numberOfQueries = numberOfQueries;
    }
    
    /**
     * @return the number of queries
     */
    public Long getNumberOfQueries(){
    	return this.numberOfQueries;
    }
}
