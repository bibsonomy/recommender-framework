/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database.params;

/**
 * Database parameter containing information about a recommendation result.
 */
public class ResultParam {

	private String resultId;
	private long queryId;
	private long settingId;
	private long recLatency;
	private double score;
	private double confidence;
	private String resultTitle;
	
	/**
	 * @return the resultId
	 */
	public String getResultId() {
		return resultId;
	}
	/**
	 * @param string the resultId to set
	 */
	public void setResultId(String string) {
		this.resultId = string;
	}
	/**
	 * @return the queryId
	 */
	public long getQueryId() {
		return queryId;
	}
	/**
	 * @param queryId2 the queryId to set
	 */
	public void setQueryId(Long queryId2) {
		this.queryId = queryId2;
	}
	/**
	 * @return the settingId
	 */
	public long getSettingId() {
		return settingId;
	}
	/**
	 * @param settingsId the settingId to set
	 */
	public void setSettingId(Long settingsId) {
		this.settingId = settingsId;
	}
	/**
	 * @return the recLatency
	 */
	public long getRecLatency() {
		return recLatency;
	}
	/**
	 * @param latency the recLatency to set
	 */
	public void setRecLatency(long latency) {
		this.recLatency = latency;
	}
	/**
	 * @return the score
	 */
	public double getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(double score) {
		this.score = score;
	}
	/**
	 * @return the confidence
	 */
	public double getConfidence() {
		return confidence;
	}
	/**
	 * @param confidence the confidence to set
	 */
	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}
	/**
	 * @return the resultTitle
	 */
	public String getResultTitle() {
		return resultTitle;
	}
	/**
	 * @param resultTitle the resultTitle to set
	 */
	public void setResultTitle(String resultTitle) {
		this.resultTitle = resultTitle;
	}
	
	
	
}
