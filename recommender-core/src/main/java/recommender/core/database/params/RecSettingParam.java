/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database.params;

/**
 * Database parameter representing the setting of a recommender.
 * 
 * @author fei
 */
public class RecSettingParam {
	private Long setting_id;
	private String recId;
	private String recDescr;
	private boolean trusted;
	private int type;
	private byte[] recMeta;
	
	/**
	 * @param recId the recommender's id
	 */
	public void setRecId(String recId) {
		this.recId = recId;
	}
	
	/**
	 * @return the recommender's id
	 */
	public String getRecId() {
		return recId;
	}
	
	/**
	 * @param recMeta metadata belonging to this recommender
	 */
	public void setRecMeta(byte[] recMeta) {
		this.recMeta = recMeta;
	}
	
	/**
	 * @return metadata belonging to this recommender
	 */
	public byte[] getRecMeta() {
		return recMeta;
	}
	
	/**
	 * @param setting_id the recommender setting's id
	 */
	public void setSetting_id(long setting_id) {
		this.setting_id = setting_id;
	}
	
	/**
	 * @return the recommender setting's id
	 */
	public long getSetting_id() {
		return setting_id;
	}
	
	/**
	 * @param recDescr a description of the recommender
	 */
	public void setRecDescr(String recDescr) {
		this.recDescr = recDescr;
	}
	
	/**
	 * @return a description of the recommender
	 */
	public String getRecDescr() {
		return recDescr;
	}

	/**
	 * @return the trusted
	 */
	public boolean isTrusted() {
		return this.trusted;
	}

	/**
	 * @param trusted the trusted to set
	 */
	public void setTrusted(boolean trusted) {
		this.trusted = trusted;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return this.type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
}
