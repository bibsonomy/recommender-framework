/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database.params;


/**
 * Database parameter for retrieving recommender data which can be used for maintenance.
 * 
 * @author bsc
 */
public class RecAdminOverview {
	
	private Long settingID;
	private Long latency;
	private String recID;
	private int local;
	private boolean active;
	
	/**
	 * @param settingID the setting id of the recommender
	 */
	public void setSettingID(Long settingID){
	    this.settingID = settingID;
	}
	
	/**
	 * @return the setting id of the recommender
	 */
	public Long getSettingID(){
		return this.settingID;
	}
	
	/**
	 * @param latency the latency of the recommender
	 */
	public void setLatency(Long latency){
		this.latency = latency;
	}
	
	/**
	 * @return the latency of the recommender
	 */
	public Long getLatency(){
		return this.latency;
	}
	
	/**
	 * @param recID the id of the recommender
	 */
	public void setRecID(String recID){
		this.recID = recID;
	}
	
	/**
	 * @return the id of the recommender
	 */
	public String getRecID(){
		return this.recID;
	}
	
	/**
	 * @return local or distant recommender
	 */
	public int getLocal() {
		return local;
	}
	
	/**
	 * @param local local or distant recommender (0 for distant, 1 for local)
	 */
	public void setLocal(int local) {
		this.local = local;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return this.active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
}
