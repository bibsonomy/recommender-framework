/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database;

import java.util.List;

import recommender.core.model.Pair;

/**
 * This interface allows the logging logic implementation
 *  to interact with the database.
 * 
 * @author lukas
 *
 */
public interface RecommenderDatabaseManager {
	
	/**
	 * Processes an insert query and returns the result.
	 * 
	 * @param query the query to execute
	 * @param param the parameter object containing information
	 * @return the result
	 */
	Object processInsertQuery(final String query, final Object param);
	
	/**
	 * Starts and executes a batch.
	 * Adds each query given as first attribute of the pair object as insert query,
	 * with the parameter object which is given as the second attribute of the pair object.
	 * 
	 * @param queryParameterMap a list of pairs of insert queries with their parameter objects
	 * @return the number of batches executed
	 */
	Integer processBatchOfInsertQueries(final List<Pair<String, Object>> queryParameterMap);
	
	/**
	 * 
	 * Queries for objects in the database.
	 * 
	 * @param objectClass the class of the expected result
	 * @param query the query to execute
	 * @param param the parameter object containing information
	 * @return the result
	 */
	<T> T processQueryForObject(final Class<T> objectClass, final String query, final Object param);
	
	/**
	 * 
	 * Queries for a list of objects in the database.
	 * 
	 * @param objectClass the class of the expected list elements
	 * @param query the query to execute
	 * @param param the parameter object containing information
	 * @return the result
	 */
	<T> List<T> processQueryForList(final Class<T> objectClass, final String query, final Object param);
	
	/**
	 * 
	 * @param query
	 * @param param
	 * @return
	 */
	List<?> processQueryForList(final String query, final Object param);
	
	/**
	 * 
	 * Processes an update query and returns the result.
	 * 
	 * @param query the query to execute
	 * @param param the parameter object containing information
	 * @return the result
	 */
	void processUpdateQuery(final String query, final Object param);
	
	/**
	 * Starts and executes a batch.
	 * Adds each query given as first attribute of the pair object as update query,
	 * with the parameter object which is given as the second attribute of the pair object.
	 * 
	 * @param queryParameterMap a list of pairs of update queries with their parameter objects
	 * @return the number of batches executed
	 */
	Integer processBatchOfUpdateQueries(final List<Pair<String, Object>> queryParameterMap);
	
	/**
	 * 
	 * Processes an delete query and returns the result.
	 * 
	 * @param objectClass the class of the expected result
	 * @param query the query to execute
	 * @param param the parameter object containing information
	 * @return the result
	 */
	void processDeleteQuery(final String query, final Object param);
	
	/**
	 * Starts and executes a batch.
	 * Adds each query given as first attribute of the pair object as delete query,
	 * with the parameter object which is given as the second attribute of the pair object.
	 * 
	 * @param queryParameterMap a list of pairs of delete queries with their parameter objects
	 * @return the number of batches executed
	 */
	Integer processBatchOfDeleteQueries(final List<Pair<String, Object>> queryParameterMap);
	
}
