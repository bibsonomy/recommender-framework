/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database;

import java.util.Random;

/**
 * Implements a service for logging recommendation statistics.
 * 
 * @author fei
 */
public class RecommenderStatisticsManager {
	/** indicates that entity identifier was not given */
	public static int UNKNOWN_ENTITYID = -1;
	private static final Random rand = new Random();
	
	/**
	 * Get id which indicates that a recommendation query was not associated with an entity.
	 * @return UNKNOWN_ENTITYID
	 */	
	public static int getUnknownEntityID() {
		return UNKNOWN_ENTITYID;
	}
	
	/**
	 * @return A new entity ID.
	 */
	public static int getNewPID() {
		return rand.nextInt(Integer.MAX_VALUE);
	}

}
