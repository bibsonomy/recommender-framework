/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package recommender.core.database;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import recommender.core.Recommender;
import recommender.core.database.params.RecAdminOverview;
import recommender.core.database.params.RecQueryParam;
import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.model.Pair;

/**
 * This interface concerns only to the internal logging and configuration logic.
 * It must not be implemented for the use of this library.
 * 
 * @author rja
 * @param <E> 
 * @param <R> 
 */
public interface DBLogic<E, R extends RecommendationResult> {
	/**
	 * Add new query to database.
	 * 
	 * @param userName user who submitted entity
	 * @param date querie's timestamp
	 * @param entity RecommendationEntity
	 * @param timeout querie's timeout value
	 * @return unique query id
	 */
	public Long addQuery(String userName, Date date, E entity, int timeout);
	
	/**
	 * adds given recommender (identified by it's id) to given query
	 * 
	 * @param qid query's id
	 * @param sid recommender's id
	 */
	public void addRecommenderToQuery(Long qid, Long sid );
	
	/**
	 * set selection strategy which was applied in given query
	 * 
	 * @param qid query's id
	 * @param rid result selector's id
	 */
	public void setResultSelectorToQuery(Long qid, Long rid );

	/**
	 * insert selector setting into table - if given setting already exists,
	 * return its id. This should always be embedded into a transaction.
	 * @param selectorInfo 
	 * @param selectorMeta 
	 * @return unique identifier for given settings
	 */
	public Long insertSelectorSetting(String selectorInfo, byte[] selectorMeta);

	/**
	 * Add id of recommender selected for given query.
	 * 
	 * @param qid query_id 
	 * @param sid recommender's setting id
	 */
	public void addSelectedRecommender(Long qid, Long sid);

	/**
	 * Add recommender's recommended results.
	 * 
	 * @param queryId unique id identifying query
	 * @param settingsId unique id identifying recommender
	 * @param recommendationResults results of a recommender
	 * @param latency 
	 * @return number of recommendations added
	 */
	public int addRecommendation(Long queryId, Long settingsId, SortedSet<R> recommendationResults, long latency);


	/**
	 * Get sorted list of results recommended in a given query by a given recommender. 
	 * This method is only used if caching the results fails.
	 * 
	 * @param qid
	 * @param sid
	 * @return results recommended in query identified by qid and recommender identified by sid
	 */
	public SortedSet<R> getRecommendations(Long qid, Long sid);

	/**
	 * Append results which were recommended in a given query by a given recommender to a given collection. 
	 * This method is only used if caching the results fails.
	 * 
	 * @param qid
	 * @param sid
	 * @param recommendationResults 
	 */
	public void getRecommendations(Long qid, Long sid, Collection<R> recommendationResults);

	/**
	 * Get sorted list of recommendation results recommended in a given query. 
	 * This method is only used if caching the results fails.
	 * 
	 * @param qid
	 * @return results recommended in query identified by qid and all recommenders 
	 */
	public SortedSet<R> getRecommendations(Long qid);

	/**
	 * Append results which are recommended in a given query to given collection 
	 * This method is only used if caching the results fails.
	 * 
	 * @param qid query id
	 * @param recommendationResults collection where recommendation results should be appended
	 */
	public void getRecommendations(Long qid, Collection<R> recommendationResults);

	/**
	 * Get (unsorted) list of selected results for a given query. 
	 * 
	 * @param qid
	 * @return results recommended in query identified by qid and all recommenders 
	 */
	public List<R> getSelectedResults(Long qid);

	/**
	 * TODO: move to test scope
	 * 
	 * Get list of all recommenders (id) which delivered results in given query.
	 * @param qid query id
	 * @return list of ids
	 */
	public List<Long> getActiveRecommenderIDs(Long qid);

	/**
	 * Get list of all recommenders (id) which were queried.
	 * @param qid query id
	 * @return list of ids
	 */
	public List<Long> getAllRecommenderIDs(Long qid);

	/**
	 * Get list of all recommenders (id) which were queried.
	 * 
	 * @param qid query id
	 * @return list of ids
	 */
	public List<Pair<Long, Long>> getRecommenderSelectionCount(Long qid);

	/**
	 * TODO: move to test scope
	 * 
	 * Return query information for given query id
	 * 
	 * @param qid querie's id
	 * @return RecQueryParam on success, null otherwise
	 */
	public RecQueryParam getQuery(Long qid);

	/**
	 * Get recommender-infos
	 * 
	 * @return recommender-info
	 * @param id recommenderID
	 */
	public RecAdminOverview getRecommenderAdminOverview(String id);
	
	/**
	 * Get the average latency of a given recommender-setting
	 * 
	 * @param sid
	 * @param numberOfQueries number of newest latency-values which will be fetched to calculate the average latency
	 * @return average latency of the recommender
	 */
	public Long getAverageLatencyForRecommender(Long sid, Long numberOfQueries);
	
	/**
	 * Activate and disable several recommenders at once.
	 * 
	 * @param activeRecs identifiers of the settings to be activated
	 * @param disabledRecs identifiers of the settings to be disabled
	 */
	public void updateRecommenderstatus(List<Long> activeRecs, List<Long> disabledRecs );
	
	/**
	 * Store selected recommended results.
	 * 
	 * @param qid query id
	 * @param rid result selector id
	 * @param result set of recommendation results
	 * 
	 * @return the number of stored results
	 */
	public int storeRecommendation(Long qid, Long rid, Collection<R> result);
	
	/**
	 * Get queryID for given entityID, user_name and date
	 * 
	 * @param user_name 
	 * @param date 
	 * @param entityID
	 * @return the query id of the given entity
	 */
	public Long getQueryForEntity(String user_name, Date date, String entityID);
	
	/**
	 *  Connect the result with recommendation.
	 *  For tags the final resource with it's tags is linked.
	 *  For items the useful recommended items are linked. 
	 * @param userName TODO
	 * @param entity the entity for which the recommendation was triggered
	 * @param result the result of the recommendation
	 */
	public void addFeedback(String userName, final E entity, final R result);

	/**
	 * @param recommender
	 * @return <code>true</code> iff the recommender is registered
	 */
	public boolean isRecommenderRegistered(final Recommender<E, R> recommender);
	
	/**
	 * registers a new recommender and activates the recommender
	 * @param recommender
	 */
	public void registerRecommender(final Recommender<E, R> recommender);
	
	/**
	 * @param recommender
	 * @return <code>true</code> iff the recommender was activated
	 */
	public boolean isRecommenderActive(final Recommender<E, R> recommender);
	
	/**
	 * @param recommender
	 * @return the id of the recommender
	 */
	public Long getRecommenderId(final Recommender<E, R> recommender);
	
	/**
	 * @return all registered remote recommenders
	 */
	public List<RecommenderConnector<E, R>> getRemoteRecommenders();
	
	/**
	 * removes the selected recommender
	 * @param recommender
	 * @return <code>true</code> if the recommender was removed
	 */
	public boolean removeRecommender(final Recommender<E, R> recommender);
}