/**
 * Recommender Core - General recommender core classes
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender;

import java.util.SortedSet;

import recommender.core.RecommendationService;
import recommender.core.Recommender;
import recommender.core.interfaces.model.RecommendationResult;

/**
 * wraps a normal {@link Recommender} to a {@link RecommendationService}
 *
 * @author dzo
 * @param <E> 
 * @param <R> 
 */
public class RecommendationServiceWrapper<E, R extends RecommendationResult> implements RecommendationService<E, R>{

	private Recommender<E, R> recommender;
	
	/**
	 * @param recommender
	 */
	public RecommendationServiceWrapper(Recommender<E, R> recommender) {
		this.recommender = recommender;
	}

	/* (non-Javadoc)
	 * @see recommender.core.RecommendationService#getRecommendationsForUser(java.lang.String, java.lang.Object)
	 */
	@Override
	public SortedSet<R> getRecommendationsForUser(String userName, E entity) {
		return this.recommender.getRecommendation(entity);
	}

	/* (non-Javadoc)
	 * @see recommender.core.RecommendationService#setFeedback(java.lang.String, java.lang.Object, recommender.core.interfaces.model.RecommendationResult)
	 */
	@Override
	public void setFeedback(String userName, E entity, R result) {
		this.recommender.setFeedback(entity, result);
	}

	/* (non-Javadoc)
	 * @see recommender.core.RecommendationService#setNumberOfResultsToRecommend(int)
	 */
	@Override
	public void setNumberOfResultsToRecommend(int numberOfResultsToRecommend) {
		this.recommender.setNumberOfResultsToRecommend(numberOfResultsToRecommend);
	}
}
