/**
 * Recommender Server - Server implementation for web service recommender
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.server.servlet;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.model.RecommendationResult;


/**
 * A recommender dispatching servlet for providing a remote recommender to BibSonomy.
 * The recommender's class name is given via the context parameter 'Recommender' and loaded 
 * during servlet's initialization.
 * 
 *  The context parameter can either be given in the servlets web.xml:
 *    <context-param>
 * 		  <param-name>Recommender</param-name>
 *	      <param-value>org.bibsonomy.recommender.tags.simple.SimpleContentBasedTagRecommender</param-value>
 *	  </context-param>
 *
 *  or in the servlet container's context configuration, for example in tomcat's 'conf/context.xml':
 *    <Parameter name="Recommender" value="org.bibsonomy.recommender.tags.simple.DummyTagRecommender" override="false"/>
 *    
 * @param <E> the recommendation entity
 * @param <R> the recommendation
 */
public class RecommenderServlet<E, R extends RecommendationResult> extends HttpServlet {
	private static final long serialVersionUID = 8662816363794082938L;
	
	/** url map for the getRecommendation method */
	private static final String METHOD_GETRECOMMENDATION = "getRecommendedTags";
	/** url map for the setFeedback method */
	private static final String METHOD_SETFEEDBACK = "setFeedback";

	
	/** the recommender */
	private RecommenderConnector<E, R> recommender;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RecommenderServlet() {
		super();
	}
	
	/**
	 * Initialize the recommender when the servlet is loaded.
	 * The recommender's classname is provided in the context parameter 'Recommender'.
	 * 
	 * @see GenericServlet
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			// instantiate recommender
			final String recName = getServletContext().getInitParameter("Recommender");
			final Class<?> recClass = Class.forName(recName);
			this.recommender = (RecommenderConnector<E, R>) recClass.newInstance();
		} catch (Exception e) {
			throw new ServletException("Could not load recommender class", e);
		}
	}

	/*
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	/*
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	//------------------------------------------------------------------------
	// private helpers
	//------------------------------------------------------------------------
	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// query entityId: recommendation's id
		

		// determine whether we should recommend or set recommender's feedback
		String methodName = request.getPathInfo();
		if (methodName != null)
			methodName = methodName.replace("/", "");
		if ((methodName == null) || (METHOD_GETRECOMMENDATION.equals(methodName))) {
			// query data: the entity for recommendation  
			final String dataString = request.getParameter(RecommenderConnector.ID_RECQUERY);
			// generate list of recommendations
			dispatchQuery(response, dataString);
		} else if (METHOD_SETFEEDBACK.equals(methodName)) {
			// query feedback: the (xml)-encoded post for feedback
			final String feedbackString = request.getParameter(RecommenderConnector.ID_FEEDBACK);
			// forward feedback to recommender
			dispatchFeedback(response, feedbackString);
		} else {
			// unknown method requested
			throw new ServletException("Method " + methodName + " not implemented." ); 
		}
	}

	/**
	 * dispatches recommender query
	 * 
	 * @param response where to write the serialized recommendation
	 * @param dataString the entity model
	 * @throws IOException 
	 */
	protected void dispatchQuery(HttpServletResponse response, String dataString) throws IOException {
		// parse the post model
		final Reader doc = new StringReader(dataString);
		final E recommendationEntity = this.recommender.getRecommendationRenderer().parseRecommendationEntity(doc);
		
		SortedSet<R> recommendations = new TreeSet<R>();
		if (recommendationEntity != null) {
			// query recommender
			recommendations = this.recommender.getRecommendation(recommendationEntity);
		} else {
			// TODO: logging?
		}
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType(this.recommender.getRecommendationRenderer().getContentType());
		this.recommender.getRecommendationRenderer().serializeRecommendationResultList(response.getWriter(), recommendations);
	}

	/**
	 * forwards final post as stored in to recommender (identified by its entityID)
	 * 
	 * @param response
	 * @param feedbackString
	 * @param entityID
	 * @throws IOException 
	 */
	private void dispatchFeedback(HttpServletResponse response, String feedbackString) throws IOException {
		// parse entity result
		final Reader doc = new StringReader(feedbackString);
		final E entityResult = this.recommender.getRecommendationRenderer().parseRecommendationEntity(doc);
		
		// query recommender
		if (entityResult != null) {
			this.recommender.setFeedback(entityResult, null); // TODO: handle result?
		}
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType(this.recommender.getRecommendationRenderer().getContentType());
		this.recommender.getRecommendationRenderer().serializeOK(response.getWriter());
	}
}
