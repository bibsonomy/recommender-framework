/**
 * Recommender Server - Server implementation for web service recommender
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.server.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.web.context.ConfigurableWebApplicationContext;

/**
 * @author dzo
 */
public class RecommendationServerInitializer implements ApplicationContextInitializer<ConfigurableWebApplicationContext> {
	private static final Log log = LogFactory.getLog(RecommendationServerInitializer.class);
	
	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.context.ApplicationContextInitializer#initialize(org.springframework.context.ConfigurableApplicationContext)
	 */
	@Override
	public void initialize(ConfigurableWebApplicationContext applicationContext) {
		registerPropertiesFile("defaultProfile", "classpath:recommendation-server.properties", applicationContext);
		registerPropertiesFile("externalProfile", "file:${user.home}" + applicationContext.getServletContext().getContextPath() + "/recommendation-server.properties", applicationContext);
	}

	private static void registerPropertiesFile(String name, String location, ConfigurableWebApplicationContext applicationContext) {
		String resolvedLocation = applicationContext.getEnvironment().resolvePlaceholders(location);
		final Resource resource = applicationContext.getResource(resolvedLocation);
		try {
			final Properties props = PropertiesLoaderUtils.loadProperties(resource);
			final PropertiesPropertySource ps = new PropertiesPropertySource(name, props);
			
			applicationContext.getEnvironment().getPropertySources().addFirst(ps);
		} catch (FileNotFoundException e) {
			log.warn("config file not found (" + resolvedLocation + ")");
		} catch (IOException e1) {
			log.error("error loading file ", e1);
		}
	}
}
