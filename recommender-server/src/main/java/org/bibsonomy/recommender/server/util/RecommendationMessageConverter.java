/**
 * Recommender Server - Server implementation for web service recommender
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.server.util;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.SortedSet;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;

/**
 * TODO: add documentation to this class
 * 
 * @author dzo
 * @param <E>
 * @param <R>
 */
public class RecommendationMessageConverter<E, R extends RecommendationResult> extends AbstractHttpMessageConverter<Object> {

	private RecommendationRenderer<E, R> renderer;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.http.converter.AbstractHttpMessageConverter#supports
	 * (java.lang.Class)
	 */
	@Override
	protected boolean supports(Class<?> clazz) {
		throw new IllegalStateException("supports called");
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.http.converter.AbstractHttpMessageConverter#canRead(java.lang.Class, org.springframework.http.MediaType)
	 */
	@Override
	public boolean canRead(Class<?> clazz, MediaType mediaType) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.http.converter.AbstractHttpMessageConverter#canWrite
	 * (java.lang.Class, org.springframework.http.MediaType)
	 */
	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return clazz != null && SortedSet.class.isAssignableFrom(clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.http.converter.AbstractHttpMessageConverter#readInternal
	 * (java.lang.Class, org.springframework.http.HttpInputMessage)
	 */
	@Override
	protected Object readInternal(Class<? extends Object> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
		throw new IllegalStateException("read not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.http.converter.AbstractHttpMessageConverter#writeInternal
	 * (java.lang.Object, org.springframework.http.HttpOutputMessage)
	 */
	@Override
	protected void writeInternal(Object t, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
		if (t instanceof SortedSet<?>) {
			@SuppressWarnings("unchecked")
			final SortedSet<R> recommendations = (SortedSet<R>) t;
			this.renderer.serializeRecommendationResultList(new OutputStreamWriter(outputMessage.getBody()), recommendations);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.http.converter.AbstractHttpMessageConverter#getSupportedMediaTypes()
	 */
	@Override
	public List<MediaType> getSupportedMediaTypes() {
		// return Collections.singletonList(MediaType.ALL);
		return MediaType.parseMediaTypes(this.renderer.getContentType());
	}

	/**
	 * @param renderer the renderer to set
	 */
	public void setRenderer(RecommendationRenderer<E, R> renderer) {
		this.renderer = renderer;
	}
}
