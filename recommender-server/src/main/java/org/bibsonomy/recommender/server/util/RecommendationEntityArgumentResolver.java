/**
 * Recommender Server - Server implementation for web service recommender
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.server.util;

import java.io.StringReader;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.model.RecommendationResult;
import recommender.core.interfaces.renderer.RecommendationRenderer;

/**
 *
 * @author dzo
 * @param <E> 
 * @param <R> 
 */
public class RecommendationEntityArgumentResolver<E, R extends RecommendationResult> implements HandlerMethodArgumentResolver {
	
	private RecommendationRenderer<E, R> renderer;
	
	/* (non-Javadoc)
	 * @see org.springframework.web.method.support.HandlerMethodArgumentResolver#supportsParameter(org.springframework.core.MethodParameter)
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return parameter.hasParameterAnnotation(RecommenderEntity.class);
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.method.support.HandlerMethodArgumentResolver#resolveArgument(org.springframework.core.MethodParameter, org.springframework.web.method.support.ModelAndViewContainer, org.springframework.web.context.request.NativeWebRequest, org.springframework.web.bind.support.WebDataBinderFactory)
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		final String feedback = webRequest.getParameter(RecommenderConnector.ID_FEEDBACK);
		if (feedback != null) {
			return this.getRecommendationEntityByData(feedback);
		}
		
		final String post = webRequest.getParameter(RecommenderConnector.ID_RECQUERY);
		if (post != null) {
			return this.getRecommendationEntityByData(post);
		}
		return null;
	}

	private E getRecommendationEntityByData(final String data) {
		return this.renderer.parseRecommendationEntity(new StringReader(data));
	}

	/**
	 * @param renderer the renderer to set
	 */
	public void setRenderer(RecommendationRenderer<E, R> renderer) {
		this.renderer = renderer;
	}
}
