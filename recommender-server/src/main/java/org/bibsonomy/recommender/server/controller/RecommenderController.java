/**
 * Recommender Server - Server implementation for web service recommender
 *
 * Copyright (C) 2013 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://dmir.uni-wuerzburg.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */
package org.bibsonomy.recommender.server.controller;

import java.util.Map;
import java.util.SortedSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.recommender.server.util.RecommenderEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import recommender.core.Recommender;
import recommender.core.interfaces.RecommenderConnector;
import recommender.core.interfaces.model.RecommendationResult;

/**
 * 
 * @author dzo
 * @param <E> 
 * @param <R> 
 */
@Controller
public class RecommenderController<E, R extends RecommendationResult> {
	private static final Log log = LogFactory.getLog(RecommenderController.class);
	
	
	private static final String BASE_PATH_RECOMMENDATION = "/" +  RecommenderConnector.METHOD_GETRECOMMENDEDTAGS;
	
	
	@Autowired
	private Map<String, Recommender<E, R>> recommenders;
	
	/**
	 * @param recommenderId 
	 * @param recommendationEntity
	 * @return the recommendation
	 */
	@ResponseBody
	@RequestMapping(value = "/{recommenderId}" + BASE_PATH_RECOMMENDATION, method = RequestMethod.POST)
	public SortedSet<R> getRecommendation(@PathVariable("recommenderId") String recommenderId, @RecommenderEntity final E recommendationEntity) {
		log.debug("handling recommendation of " + recommendationEntity + " for" + recommenderId);
		
		final Recommender<E, R> recommender = getRecommender(recommenderId);
		return recommender.getRecommendation(recommendationEntity);
	}
	
	/**
	 * the default recommender
	 * @param recommendationEntity
	 * @return the recommendation
	 */
	@ResponseBody
	@RequestMapping(value = "BASE_PATH_RECOMMENDATION", method = RequestMethod.POST)
	public SortedSet<R> getDefaultRecommendation(@RecommenderEntity final E recommendationEntity) {
		final String recommenderId;
		if (this.recommenders.size() == 1) {
			recommenderId = recommenders.keySet().iterator().next();
		} else {
			recommenderId = "default";
		}
		
		return getRecommendation(recommenderId, recommendationEntity);
	}

	private Recommender<E, R> getRecommender(String recommenderId) {
		final Recommender<E, R> recommender = this.recommenders.get(recommenderId);
		if (recommender == null) {
			throw new IllegalStateException("recommender " + recommenderId + " not found");
		}
		return recommender;
	}
	
	/**
	 * @param recommendationEntity
	 */
	@RequestMapping(value = "/" + RecommenderConnector.METHOD_SETFEEDBACK, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public void setFeedBack(@RecommenderEntity final E recommendationEntity) {
		log.debug("handling feedback for " + recommendationEntity);
		// this.recommender.setFeedback(recommendationEntity, null);
	}
}
